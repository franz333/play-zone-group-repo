

<!-- PROJECT LOGO -->
<br />
<div align="left" id="top">
  <a href="https://www.technobel.be/fr/">
    <img src="https://www.technobel.be/images/technobel_logo_hor.png" alt="Logo" width=300>
  </a>

  <h2 align="left">.Net Developer 9 months training</h2>
  <h3>Play-Zone: Learn by doing</h3>
  <br />
</div>

<!-- TABLE OF CONTENTS -->
Table of contents
<ol>
  <li><a href="#playzone">Play-Zone</a></li>
  <li><a href="#digital-solution">A Digital Solution To Collect, Map And Share Observations Of Biodiversity</a></li>
    <ol>
      <li><a href="#mobile-app">A Mobile App</a></li>
      <li><a href="#web-app">A Web App</a></li>
      <li><a href="#api">An API</a></li>
    </ol>
  <li><a href="#me">My contributions</a></li>
  <li><a href="#tech">Technologies</a></li>
      <ol>
      <li><a href="#built-with">Built With</a></li>
    </ol>
  <li><a href="#contact">Contact</a></li>
  <li><a href="#acknowledgments">Acknowledgments</a></li>
</ol>
<br />

<!-- THE CONTEXT OF PLAYZONE -->
<h2 id="playzone">1. Play-Zone</h2>

The Play-Zone is a 1 month group project to which each student can take part at the end of the 9 months training provided by Technobel - Training center chosen by [Le Forem](https://www.leforem.be/centres-de-competence/techno-bel.html). 

It gathers different classes of students who attended a training (UX Designer, Infrastructure, Developers, etc.) around a project that they build working together. This initiative is done in partnership with a company that faces real problems that could be solved thanks to a digital solution. In 2019, that company was a natural park called [Le Domaine de Chevetogne](https://www.domainedechevetogne.be/).

Several groups were created and each group had to come up with one or several digital solution(s).
<br />
<p align="center"><a href="#top">-- Back to top --</a></p>

<!-- ABOUT THE PROJECT -->
<h2 id="digital-solution">2. A Digital Solution To Collect, Map And Share Observations Of Biodiversity</h2>

I was part of the "Environment" group which was composed of 3 persons. We were in charge of helping the park gather observations of biodiversity done by its visitors or its staff within the premises of the natutal park.

The client wanted a mobile application for its users to allow them to share observations with the park. Ideally the application should be able to recognise a plant or an animal from a picture uploaded by the user and register the observation details in a database (description, picture and coordinates ideally). The staff would then receive a notification, review and control the observation before final recording in the database. 
Also the application should provide its users with information about the park and its species. 

The ambitious solution that was imagined was to create 3 digital tools in order to answer the client's requirements: a mobile application, a web application and an API.

###   1. <a id="mobile-app"></a>A Mobile App ###

One of my colleague was in charge of the mobile application. She wanted to develop it with Ionics but finally never even started the project due to several problems: dependency on an API (ex: [INaturalist](https://www.inaturalist.org/)), low or no Internet network in the park, human factors, lack of technical knowledge.

###   2. <a id="web-app"></a>A Web App ###

I was in charge of developing a Web Application and the database in order to provide the staff with a dashboard to monitor, review,validate and record all the observations in a database. 

###   3. <a id="api"></a>An API ###

A web scrapper was developed by another colleague to feed the database with some data ready to use like taxonomies, species, images, etc..She was also in charge of creating the API responsible to provide the mobile application with the necessary methods and access to send information collected by users to the database.

<br />
<p align="center"><a href="#top">-- Back to top --</a></p>

<h2 id="me">3. My contributions</h2>

Being in charge of the database, I made a research about the topic and tried to build the entity–relationship model ([image here](/Documentation/db_modelpng.png)). I created the database from it (manually, not with Entity Framework). To help me in this task, I used an excel file provided by the client that was containing some observations and I sorted it with the help of information found on [INaturalist](https://www.inaturalist.org/). 

I took contact with INaturalist but they did not allow us to use their [API](https://www.inaturalist.org/pages/api+reference) in an external project.

I also created the web application with differents users's right as only certain person in the staff were able to validate observations.

Finally, as I had already done the database and as my colleague in charge of the API could not (or did not bother) develop the API, I created it. 
<br />
<p align="center"><a href="#top">-- Back to top --</a></p>

<!-- TECHNOLOGIES -->
<h2 id="tech">4. Technologies</h2>

<h3 id="built-with">1. Built With</h3>

This section list major tools/frameworks/libraries used to bootstrap my project. 

* [![DBMain][DBMain]][DBMain-url]
* [![MicrosoftSQLServer][MicrosoftSQLServer]][MicrosoftSQLServer-url]
* [![VisualStudio][VisualStudio]][VisualStudio-url]
* [![CSharp][CSharp]][CSharp-url]
* [![Dotnet][DotNet]][DotNet-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![JQuery][JQuery.com]][JQuery-url]
* [![BitBucket][BitBucket]][BitBucket-url]

<p align="center"><a href="#top">-- Back to top --</a></p>

<br />
<br />

<!-- CONTACT -->
<h2 id="contact">4. Contact</h2>

[Linkedin](https://www.linkedin.com/in/francoisduroy/)

<br />
<br />

<!-- ACKNOWLEDGMENTS -->
<h2 id="acknowledgments">5. Acknowledgments</h2>

* [Img Shields](https://shields.io)
* [GitHub Pages](https://pages.github.com)
* [OthNeilDrew Readme Template](https://github.com/othneildrew/Best-README-Template)

<p align="center"><a href="#top">-- Back to top --</a></p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com

[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 

[CSharp]: https://img.shields.io/badge/c%20sharp-239120?style=for-the-badge&logo=csharp&logoColor=white
[CSharp-url]: https://learn.microsoft.com/en-us/dotnet/csharp/

[DotNet]: https://img.shields.io/badge/.NET-512BD4?style=for-the-badge&logo=dotnet&logoColor=white
[DotNet-url]: https://dotnet.microsoft.com/

[MicrosoftSQLServer]: https://img.shields.io/badge/microsoft%20sql%20server-CC2927?style=for-the-badge&logo=microsoftsqlserver&logoColor=white
[MicrosoftSQLServer-url]: https://www.microsoft.com/en-us/sql-server/

[Invision]:https://img.shields.io/badge/invision-FF3366?style=for-the-badge&logo=invision&logoColor=white
[Invision-url]: https://www.invisionapp.com/

[DBMain]: https://img.shields.io/badge/DB%20Main-DB%20Main-red
[DBMain-url]: https://www.db-main.eu/

[VisualStudio]: https://img.shields.io/badge/Visual%20Studio-5C2D91?style=for-the-badge&logo=visualstudio&logoColor=white
[VisualStudio-url]: https://visualstudio.microsoft.com/

[BitBucket]: https://img.shields.io/badge/Bitbucket-0052CC?style=for-the-badge&logo=bitbucket&logoColor=white
[BitBucket-url]: https://bitbucket.org/