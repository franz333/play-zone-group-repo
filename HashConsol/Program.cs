﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HashConsol
{
    class Program
    {
        static void Main(string[] args)
        {
            HashAlgorithm hashAlgorithm;
            string keyControl;

            hashAlgorithm = new SHA256Managed();

            keyControl = "environnementKey";

            // Concaténation du pwd + valeur du keyControl avant hashage
            // Le fait de formatter (UTF32) côté "logiciel" permet d'avoir toutjours un hashage au même format
            // et ce indépendemment de la bécane de l'utilisateur
            // UTF32 est moins utilisé que UT8 du coup, on diminue le risque de faille de sécurité
            Console.WriteLine(hashAlgorithm.ComputeHash(Encoding.UTF32.GetBytes("Test1234=" + keyControl)));
            Console.ReadKey();
        }
    }
}
