--------------------------------------
--------------------------------------
-- TRIGGERS --
--------------------------------------
--------------------------------------

USE [Playzone_DB_Env]

----------------------
-- USERS --
----------------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [UserInsertControl]
   ON [User]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	insert into [User] (LastName, [Name], [Email], [Login], Pwd, RoleId) 
		select LastName, [Name], [Email], [Login], Pwd, RoleId 
			from Inserted 
				where  Pwd not in (select Pwd from [User]) 
					and [Login] not in (select [Login] from [User]);
END

------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER UserUpdateControl
   ON [User]
   INSTEAD OF UPDATE
AS 
BEGIN	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	update [User] 
		set 
			LastName = (select LastName from inserted), 
			[Name] = (select [Name] from inserted), 
			Email = (select Email from inserted), 
			[Login] = (select [Login] from inserted),
			Pwd = (select Pwd from inserted), 
			Active = (select Active from inserted), 
			RoleId = (select RoleId from inserted)			
			Where (select Id from inserted) = Id 
			And ((select [Login] from deleted where deleted.Id = Id) = (Select [Login] from inserted where inserted.Id = Id) 
				or (Select [Login] from inserted where inserted.Id = Id) not in (Select [Login] from [User])); 
END

----------------------
-- SPECIES --
----------------------

GO
/****** Object:  Trigger [dbo].[DeleteSpecies]    Script Date: 10-12-19 11:40:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[DeleteSpecies]
   ON [dbo].[Species]
   INSTEAD OF DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete From Img Where SpeciesId in (Select Id from deleted); 
	Delete From Observation Where SpeciesId in (Select Id from deleted);
	Delete from Species where Species.Id in (Select deleted.Id from deleted)
END

-------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER SpeciesInsertControl
   ON Species
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	insert into Species (LatinName, [Name], [Description], PeriodBegin, PeriodEnd, PublisherId, GenderId, StatusId)  
		select LatinName, [Name], [Description], PeriodBegin, PeriodEnd, PublisherId, GenderId, StatusId
		from Inserted 
		where LatinName not in (select LatinName from Species);
		-- on d�place la ligne suivante qui est comment�e dans la proc�dure stock�e sinon il renvoie pas l'id
		SELECT Species.Id from Species WHERE @@ROWCOUNT > 0 AND Species.Id = SCOPE_IDENTITY();
END


insert into Family (LatinName, [Name], OrderId) 
    select LatinName, [Name], OrderId 
    from Inserted 
    where LatinName not in (select LatinName from Family);
    select Id from Family WHERE LatinName = (select LatinName from Inserted)

sp_insert: 
INSERT INTO Family (LatinName, [Name], OrderId)
    VALUES (@LatinName, @Name, @OrderId)

-------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER SpeciesUpdateControl
   ON Species
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update Species 
		set 
			LatinName = (select LatinName from inserted), 
			[Name] = (select [Name] from inserted), 
			[Description] = (select [Description] from inserted), 
			PeriodBegin = (select PeriodBegin from inserted),
			PeriodEnd = (select PeriodEnd from inserted), 
			PublisherId = (select PublisherId from inserted), 
			GenderId = (select GenderId from inserted),
			StatusId = (select StatusId from inserted)
			Where (select Id from inserted) = Id 
			And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where inserted.Id = Id) 
				or (Select LatinName from inserted where inserted.Id = Id) not in (Select LatinName from Species)); 
END


----------------------
-- KINGDOM --
----------------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER KingdomInsertControl
   ON Kingdom
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	insert into Kingdom (LatinName, [Name]) select LatinName, [Name] 
	from Inserted
	where LatinName not in (select LatinName from Kingdom);
END

------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER KingdomUpdateControl
   ON Kingdom
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update Kingdom
		set 
			LatinName = (select LatinName from inserted), 
			[Name] = (select [Name] from inserted) 
				Where (select Id from inserted) = Id 
					And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where inserted.Id = Id) 
						or (Select LatinName from inserted where inserted.Id = Id) not in (Select LatinName from Species)); 
END

----------------------
-- BRANCH --
----------------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[BranchInsertControl]
   ON [dbo].[Branch]
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into Branch (LatinName, [Name], KingdomId) select LatinName, [Name], KingdomId 
	from Inserted 
	where LatinName not in (select LatinName from Branch);
END

-----

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER BranchUpdateControl
   ON Branch
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update Branch
		set 
			LatinName = (select LatinName from inserted), 
			[Name] = (select [Name] from inserted), 
			kingdomId = (select kingdomId from inserted)
				Where (select Id from inserted) = Id 
					And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where inserted.Id = Id) 
						or (Select LatinName from inserted where inserted.Id = Id) not in (Select LatinName from Species)); 
END

----------------------
-- SUBBRANCH --
----------------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER SubBranchInsertControl
   ON SubBranch
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	insert into SubBranch (LatinName, [Name],BranchId) 
	select LatinName, [Name], BranchId 
	from Inserted 
	where LatinName not in (select LatinName from SubBranch);
END

-----

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER SubBranchUpdateControl
   ON SubBranch
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update SubBranch
		set 
			LatinName = (select LatinName from inserted), 
			[Name] = (select [Name] from inserted), 
			BranchId = (select BranchId from inserted)
				Where (select Id from inserted) = Id 
					And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where inserted.Id = Id) 
						or (Select LatinName from inserted where inserted.Id = Id) not in (Select LatinName from Species)); 
END

----------------------
-- CLASS --
----------------------

---- Contr�le qu'on ins�re bien une classe qui n'existe pas
-- et que soit BranchId, soit SubBranchId est rempli
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER ClassInsertControl
   ON Class
   INSTEAD OF INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	insert into Class (LatinName, [Name], BranchId, SubBranchId) 
	
	select LatinName, [Name], 
		case when SubBranchId is not null then Null 
			else BranchId END, SubBranchId
	from Inserted 
	
	where LatinName not in (select LatinName from Class) and 
		case when (BranchId IS NULL and SubBranchId IS NULL) then 0 else 1 END = 1;
END

-----

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER ClassUpdateControl
   ON Class
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update Class 
		set 
			LatinName = (select LatinName from inserted),
			[Name] = (select Name from inserted),
			BranchId = (select case when SubBranchId is not null then null else BranchId END from inserted),
			SubBranchId = (select SubBranchId from inserted)
				Where (select Id from inserted) = Id
					And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where Inserted.Id = Id))
					and	(case when BranchId IS NULL and SubBranchId IS NULL then 0 else 1 END = 1);
END

----------------------
-- ORDER --
----------------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER OrderInsertControl
   ON [Order]
   INSTEAD OF INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	insert into [Order] (LatinName, [Name], ClassId) 
	select LatinName, [Name], ClassId 
	from Inserted 
	where LatinName not in (select LatinName from [Order]);
END

------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER OrderUpdateControl
   ON [Order]
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update [Order] 
		set 
			LatinName = (select LatinName from inserted), 
			[Name] = (select [Name] from inserted), 
			ClassId = (select ClassId from inserted)
				Where (select Id from inserted) = Id 
					And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where inserted.Id = Id) 
						or (Select LatinName from inserted where inserted.Id = Id) not in (Select LatinName from Species)); 
END

----------------------
-- FAMILY --
----------------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER FamilyInsertControl
   ON Family
   INSTEAD OF INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	insert into Family (LatinName, [Name], OrderId) 
	select LatinName, [Name], OrderId 
	from Inserted 
	where LatinName not in (select LatinName from Family);
END

-----

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER FamilyUpdateControl
   ON Family
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update Family
		set 
			LatinName = (select LatinName from inserted), 
			[Name] = (select [Name] from inserted), 
			OrderId = (select OrderId from inserted)
				Where (select Id from inserted) = Id 
					And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where inserted.Id = Id) 
						or (Select LatinName from inserted where inserted.Id = Id) not in (Select LatinName from Species)); 
END


----------------------
-- GENDER --
----------------------

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER GenderInsertControl
   ON Gender
   INSTEAD OF INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	insert into Gender (LatinName, [Name], FamilyId) 
	select LatinName, [Name], FamilyId 
	from Inserted 
	where LatinName not in (select LatinName from Gender);
END

----

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER GenderUpdateControl
   ON Gender
   INSTEAD OF UPDATE
AS 
BEGIN	
	SET NOCOUNT ON;
	update Gender
		set 
			LatinName = (select LatinName from inserted), 
			[Name] = (select [Name] from inserted), 
			FamilyId = (select FamilyId from inserted)
				Where (select Id from inserted) = Id 
					And ((select LatinName from deleted where deleted.Id = Id) = (Select LatinName from inserted where inserted.Id = Id) 
						or (Select LatinName from inserted where inserted.Id = Id) not in (Select LatinName from Species)); 
END