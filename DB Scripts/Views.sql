use Playzone_DB_Env_v10;

GO
CREATE VIEW v_User
 AS
SELECT 
	U.Id,
	U.[Name],
	U.LastName,
	U.Email,
	U.[Login],
	U.Pwd,
	U.Active,
	R.[Type] as 'RoleType',
	R.Id as 'RoleId'
FROM [User] as U
JOIN [ROLE] as R on R.Id = U.RoleId


GO
CREATE VIEW v_Observation
AS
select 
	O.*,
	I.[Data] as 'Img',
	I.Id as 'ImgId',
	U.[Name] as 'ValidatorName',
	U.LastName as 'ValidatorLastName',
	U2.[Name] as 'PublisherName',
	U2.LastName as 'PublisherLastName',
	St.Type as 'StatusType',
	R.[Type] as 'RoleType'
from Observation O
left join Img I on I.ObservationId = O.Id
left join [User] U on U.Id = O.ValidatorId
left join [User] U2 on U2.Id = O.PublisherId
left join [Role] R on R.Id = O.PublisherId
left join [Status] St on St.Id = O.StatusId

select * from Kingdom

GO
CREATE VIEW v_Species
AS
SELECT 
	S.*,
	St.[Type] as 'StatusType',	
	G.Id as 'GenreId',
	G.LatinName as 'GenderLatinName',
	G.Name as 'GenderName',
	G.FamilyId as 'GenderFamilyId',
	F.Id as 'FamilyId',
	F.LatinName as 'FamilyLatinName',
	F.Name as 'FamilyName',
	O.Id as 'OrderId',
	O.LatinName as 'OrderLatinName',
	O.Name as 'OrderName',
	O.ClassId as 'OrderClassId',
	C.Id as 'ClassId',
	C.LatinName as 'ClassLatinName',
	C.Name as 'ClassName',
	C.BranchId as 'ClassBranchId',
	C.SubBranchId as 'ClassSubBranchId',
	Sb.Id as 'SubBranchId',
	Sb.LatinName as 'SubBranchLatinName',
	Sb.Name as 'SubBranchName',
	Sb.BranchId as 'SubBranchBranchId',
	B.Id as 'BranchId',
	B.LatinName as 'BranchLatinName',
	B.Name as 'BranchName',
	B.KingdomId as 'BranchKingdomId',
	K.Id as 'KingdomId',
	K.LatinName as 'KingdomLatinName',
	K.Name as 'KingdomName',
	U.Name as 'PublisherName',
	U.LastName as 'PublisherLastName'
from Species S
inner join [Status] St on St.Id = S.StatusId 
inner join [User] as U on U.Id = S.PublisherId
inner join Gender G on G.Id = S.GenderId
inner join Family F on F.Id = G.FamilyId
inner join [Order] O on O.Id = F.OrderId
inner join Class C on C.Id = O.ClassId	
inner join SubBranch Sb on Sb.Id = C.SubBranchId
inner join Branch B on B.Id = C.BranchId or B.Id = Sb.BranchId
	And Case 
		When C.SubBranchId = 1
			then 0
		else 1
	END = 1
inner join Kingdom K on K.Id = B.KingdomId
Where S.Id <> 1;

GO
CREATE VIEW v_Species_With_Unknown
AS
SELECT 
	S.*,
	St.[Type] as 'StatusType',	
	G.Id as 'GenreId',
	G.LatinName as 'GenderLatinName',
	G.Name as 'GenderName',
	G.FamilyId as 'GenderFamilyId',
	F.Id as 'FamilyId',
	F.LatinName as 'FamilyLatinName',
	F.Name as 'FamilyName',
	O.Id as 'OrderId',
	O.LatinName as 'OrderLatinName',
	O.Name as 'OrderName',
	O.ClassId as 'OrderClassId',
	C.Id as 'ClassId',
	C.LatinName as 'ClassLatinName',
	C.Name as 'ClassName',
	C.BranchId as 'ClassBranchId',
	C.SubBranchId as 'ClassSubBranchId',
	Sb.Id as 'SubBranchId',
	Sb.LatinName as 'SubBranchLatinName',
	Sb.Name as 'SubBranchName',
	Sb.BranchId as 'SubBranchBranchId',
	B.Id as 'BranchId',
	B.LatinName as 'BranchLatinName',
	B.Name as 'BranchName',
	B.KingdomId as 'BranchKingdomId',
	K.Id as 'KingdomId',
	K.LatinName as 'KingdomLatinName',
	K.Name as 'KingdomName',
	U.Name as 'PublisherName',
	U.LastName as 'PublisherLastName'
from Species S
inner join [Status] St on St.Id = S.StatusId 
inner join [User] as U on U.Id = S.PublisherId
inner join Gender G on G.Id = S.GenderId
inner join Family F on F.Id = G.FamilyId
inner join [Order] O on O.Id = F.OrderId
inner join Class C on C.Id = O.ClassId	
inner join SubBranch Sb on Sb.Id = C.SubBranchId
inner join Branch B on B.Id = C.BranchId or B.Id = Sb.BranchId
	And Case 
		When C.SubBranchId = 1
			then 0
		else 1
	END = 1
inner join Kingdom K on K.Id = B.KingdomId


GO
CREATE VIEW v_API_Species
AS
SELECT 
	S.*,
	St.[Type] as 'StatusType',	
	G.Id as 'GenreId',
	G.LatinName as 'GenderLatinName',
	G.Name as 'GenderName',
	G.FamilyId as 'GenderFamilyId',
	F.Id as 'FamilyId',
	F.LatinName as 'FamilyLatinName',
	F.Name as 'FamilyName',
	O.Id as 'OrderId',
	O.LatinName as 'OrderLatinName',
	O.Name as 'OrderName',
	O.ClassId as 'OrderClassId',
	C.Id as 'ClassId',
	C.LatinName as 'ClassLatinName',
	C.Name as 'ClassName',
	C.BranchId as 'ClassBranchId',
	C.SubBranchId as 'ClassSubBranchId',
	Sb.Id as 'SubBranchId',
	Sb.LatinName as 'SubBranchLatinName',
	Sb.Name as 'SubBranchName',
	Sb.BranchId as 'SubBranchBranchId',
	B.Id as 'BranchId',
	B.LatinName as 'BranchLatinName',
	B.Name as 'BranchName',
	B.KingdomId as 'BranchKingdomId',
	K.Id as 'KingdomId',
	K.LatinName as 'KingdomLatinName',
	K.Name as 'KingdomName',
	U.Name as 'PublisherName',
	U.LastName as 'PublisherLastName'
from Species S
inner join [Status] St on St.Id = S.StatusId 
inner join [User] as U on U.Id = S.PublisherId
inner join Gender G on G.Id = S.GenderId
inner join Family F on F.Id = G.FamilyId
inner join [Order] O on O.Id = F.OrderId
inner join Class C on C.Id = O.ClassId	
inner join SubBranch Sb on Sb.Id = C.SubBranchId
inner join Branch B on B.Id = C.BranchId or B.Id = Sb.BranchId
	And Case 
		When C.SubBranchId = 1
			then 0
		else 1
	END = 1
inner join Kingdom K on K.Id = B.KingdomId
Where S.Id <> 1;