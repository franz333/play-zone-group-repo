use Playzone_DB_Env_v10;

-----------------------------
-- ROLES --
-----------------------------
Go
CREATE PROCEDURE sp_GetRole
	@Id int
AS
 BEGIN
	SELECT * FROM [Role]
	WHERE Id = @Id
END

GO
CREATE PROCEDURE sp_LoginUser
	@Login nvarchar(100),
	@Pwd varbinary(8000)
AS
 BEGIN
	SELECT * FROM [User] U
	WHERE U.[Login] = @Login and U.Pwd = @Pwd
END

-------------------------

-----------------------------
-- USER --
-----------------------------

Go
CREATE PROCEDURE sp_InsertUser
	@Name nvarchar(100),
	@LastName nvarchar(100),
	@Email nvarchar(150),
	@Login nvarchar(100),
	@Pwd varbinary(8000),
	@Active bit,
	@RoleId int
AS
 BEGIN
	INSERT INTO [User] (Name, LastName, Email, [Login], Pwd, Active, RoleId)
	OUTPUT inserted.Id
	VALUES (@Name, @LastName, @Email, @Login, @Pwd, @Active, @RoleId)
 END

----------------------
Go
CREATE PROCEDURE sp_DeleteUser
	@Id int
AS
 BEGIN
	DELETE FROM [User] where Id = @Id;
END

----------------------
Go
CREATE PROCEDURE sp_UpdateUser
	@Id int,
	@Name nvarchar(100),
	@LastName nvarchar(100),
	@Email nvarchar(150),
	@Login nvarchar(100),
	@Pwd varbinary(8000),
	@Active bit,
	@RoleId int
AS
 BEGIN	
	UPDATE [User]
	 SET Name = @Name,
	     Lastname = @Lastname,
	     Email = @Email,
		 [Login] = @Login,
	     Pwd = @Pwd,
		 Active = @Active,
		 RoleId = @RoleId
	WHERE Id = @Id
 END

----------------------
Go
CREATE PROCEDURE sp_DeactivateUser
	@Id int
AS
 BEGIN	
	UPDATE [User]
	 SET Active = 0
	WHERE Id = @Id
 END

 Go
CREATE PROCEDURE p_ActivateUser
	@Id int
AS
 BEGIN	
	UPDATE [User]
	 SET Active = 1
	WHERE Id = @Id
 END

----------------------

-----------------------------
-- SPECIES --
-----------------------------
Go
CREATE PROCEDURE sp_InsertSpecies
	@Description nvarchar(1000),
	@GenderId int,
	@LatinName nvarchar(100), 
	@Name nvarchar(100), 
	@PeriodBegin int, 
	@PeriodEnd int, 
	@PublisherId int, 
	@StatusId int
AS
 BEGIN
	INSERT INTO Species ([Description], GenderId, LatinName, Name, PeriodBegin, PeriodEnd, PublisherId, StatusId)	
	VALUES (@Description, @GenderId, @LatinName, @Name, @PeriodBegin, @PeriodEnd, @PublisherId, @StatusId);
	SELECT Species.Id from Species WHERE @@ROWCOUNT > 0 AND Species.Id = SCOPE_IDENTITY();
END

------
Go
CREATE PROCEDURE sp_UpdateSpecies
	@Id int,
	@Description nvarchar(1000),
	@GenderId int,
	@LatinName nvarchar(100), 
	@Name nvarchar(100), 
	@PeriodBegin int, 
	@PeriodEnd int, 
	@PublisherId int, 
	@StatusId int
AS
 BEGIN	
	UPDATE Species
	 SET 
		 [Description] = @Description,
		 GenderId = @GenderId,
		 LatinName = @LatinName,
		 Name = @Name,
	     PeriodBegin = @PeriodBegin,
		 PeriodEnd = @PeriodEnd,
	     PublisherId = @PublisherId,
		 StatusId = @StatusId
	WHERE Id = @Id
 END

------
-- Last Added

GO
CREATE PROCEDURE sp_DeleteSpecies
	@Id int
AS
 BEGIN
	DELETE FROM Species
	WHERE Id = @Id
END

----
-- Last Added

GO 
CREATE PROCEDURE sp_GetAllSpeciesByStatusId
	@StatusId int
AS
 BEGIN
	SELECT * FROM v_Species
	WHERE StatusId = @StatusId
END

----
-- Last Added

GO 
CREATE PROCEDURE sp_GetAllSpeciesByPublisherId
	@PublisherId int
AS
 BEGIN
	SELECT * FROM v_Species
	WHERE PublisherId = @PublisherId
END

--EXEC sp_GetAllSpeciesByPublisherId @PublisherId = 1;

-----
-- Last Added 

GO
CREATE PROCEDURE sp_GetAllSpeciesByPeriod
	@PeriodBegin int,
	@PeriodEnd int
AS
 BEGIN
	SELECT * FROM v_Species
	WHERE PeriodBegin = @PeriodBegin AND PeriodEnd = @PeriodEnd
END

--EXEC sp_GetAllSpeciesByPeriod @PeriodBegin = 9, @PeriodEnd = 4;

--GO
--ALTER PROCEDURE sp_GetAllSpeciesByMonth
--	@Month int
--AS
-- BEGIN
--	SELECT CASE WHEN PeriodBegin > PeriodEnd
--			THEN (Select LatinName From Species Where @Month between PeriodEnd and PeriodBegin)
--			ELSE CASE WHEN PeriodEnd > PeriodBegin
--					THEN 's'END --(Select * from Species Where @Month between PeriodBegin and PeriodEnd) ELSE null END
--			END
--		FROM Species;
--END

GO
CREATE PROCEDURE sp_GetAllSpeciesByKingdomId
	@KingdomId int
AS
 BEGIN
	SELECT * FROM v_Species S
	WHERE S.KingdomId = @KingdomId;
END
EXEC sp_GetAllSpeciesByKingdomId @KingdomId = 2;

GO
CREATE PROCEDURE sp_GetAllSpeciesByBranchId
	@BranchId int
AS
 BEGIN
	SELECT * FROM v_Species	S
	WHERE S.BranchId = @BranchId
END

GO
CREATE PROCEDURE sp_GetAllSpeciesBySubBranchId
	@SubBranchId int
AS
 BEGIN
	SELECT * FROM v_Species S
	WHERE S.SubBranchId = @SubBranchId
END

--EXEC sp_GetAllSpeciesBySubBranchId @SubBranchId = 1;

GO
CREATE PROCEDURE sp_GetAllSpeciesByClassId
	@ClassId int
AS
 BEGIN
	SELECT * FROM v_Species S
	WHERE S.ClassId = @ClassId
END
--EXEC sp_GetAllSpeciesByClassId @ClassId = 1;

GO
CREATE PROCEDURE sp_GetAllSpeciesByOrderId
	@OrderId int
AS
	BEGIN
		SELECT * FROM v_Species S
		WHERE S.OrderId = @OrderId
END
--EXEC sp_GetAllSpeciesByOrderId @OrderId = 1;

GO
CREATE PROCEDURE sp_GetAllSpeciesByFamilyId
	@FamilyId int
AS
 BEGIN
	SELECT * FROM v_Species S
	WHERE S.FamilyId = @FamilyId
END
--EXEC sp_GetAllSpeciesByFamilyId @FamilyId = 1;

GO
CREATE PROCEDURE sp_GetAllSpeciesByGenderId
	@GenderId int
AS
	BEGIN
		SELECT * FROM v_Species S
		WHERE S.GenderId  = @GenderId
END
--EXEC sp_GetAllSpeciesByGenderId @GenderId = 2;



-----------------------------
-- KINGDOM --
-----------------------------
Go
CREATE PROCEDURE sp_GetKingdom
	@Id int
AS
 BEGIN
	SELECT * FROM Kingdom
	WHERE Id = @Id
END

-----
Go
CREATE PROCEDURE sp_InsertKingdom
	@LatinName nvarchar(100),
	@Name nvarchar(100)
AS
 BEGIN
	INSERT INTO Kingdom (LatinName, Name)
	VALUES (@LatinName, @Name)
	SELECT Kingdom.Id from Kingdom WHERE @@ROWCOUNT > 0 AND Kingdom.Id = SCOPE_IDENTITY();
END

-- Last Added
GO
CREATE PROCEDURE sp_DeleteKingdom
	@Id int
AS
 BEGIN
	DELETE FROM Kingdom
	WHERE Id = @Id
END

----

GO 
CREATE PROCEDURE sp_UpdateKingdom
	@Id int,
	@LatinName nvarchar(100),
	@Name nvarchar(100)
AS
 BEGIN
	UPDATE Kingdom
		SET
		 LatinName = @LatinName,
		 [Name] = @Name
	WHERE Id = @Id
END

-----------------------------
-- BRANCH --
-----------------------------
Go
CREATE PROCEDURE sp_GetBranch
	@Id int
AS
 BEGIN
	SELECT * FROM Branch
	WHERE Id = @Id
END

--------
Go
CREATE PROCEDURE sp_GetAllBranchByKingdomId
	@kingdomId int
AS
 BEGIN
	SELECT * FROM Branch
	WHERE KingdomId = @kingdomId
END

--------
Go
CREATE PROCEDURE sp_InsertBranch
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@KingdomId int
AS
 BEGIN
	INSERT INTO Branch (LatinName, [Name], KingdomId)
	VALUES (@LatinName, @Name, @KingdomId);
	SELECT Branch.Id from Branch WHERE @@ROWCOUNT > 0 AND Branch.Id = SCOPE_IDENTITY();
END

----

-- Last Added
GO
CREATE PROCEDURE sp_UpdateBranch
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@KingdomId int,
	@Id int
AS
 BEGIN
	UPDATE Branch
		SET 
			LatinName = @LatinName,
			[Name] = @Name,
			KingdomId = @KingdomId
	WHERE Id = @Id;
END

----

-- Last Added
GO
CREATE PROCEDURE sp_DeleteBranch
	@Id int
AS
 BEGIN
	DELETE FROM Branch
	WHERE Id = @Id
END

-----------------------------
-- SUBBRANCH --
-----------------------------
Go
CREATE PROCEDURE sp_GetSubBranch
	@Id int
AS
 BEGIN
	SELECT * FROM SubBranch
	WHERE Id = @Id
END

--------
Go
CREATE PROCEDURE sp_GetAllSubBranchByBranchId
	@BranchId int
AS
 BEGIN
	SELECT * FROM SubBranch
	WHERE BranchId = @BranchId
END
--EXEC sp_GetAllSubBranchByBranchId @BranchId = 6;

------
Go
CREATE PROCEDURE sp_InsertSubBranch
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@BranchId int
AS
BEGIN
 INSERT INTO SubBranch (LatinName, Name, BranchId)
 VALUES (@LatinName, @Name, @BranchId);
 SELECT SubBranch.Id from SubBranch WHERE @@ROWCOUNT > 0 AND SubBranch.Id = SCOPE_IDENTITY();
END

-- Last Added
GO
CREATE PROCEDURE sp_DeleteSubBranch
	@Id int
AS
 BEGIN
	DELETE FROM SubBranch
	WHERE Id = @Id
END

-- Last Added

GO
CREATE PROCEDURE sp_UpdateSubBranch
	@Id int,
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@BranchId int
AS
 BEGIN
	UPDATE SubBranch
	SET
		LatinName = @LatinName,
		[Name] = @Name,
		BranchId = @BranchId
	WHERE Id = @Id
END

-----------------------------
-- CLASS --
-----------------------------
Go
CREATE PROCEDURE sp_GetClass
	@Id int
AS
 BEGIN
	SELECT * FROM Class
	WHERE Id = @Id
END

-----
Go
CREATE PROCEDURE sp_GetAllClassByBranchId
	@BranchId int
AS
 BEGIN 
	SELECT * FROM Class
	WHERE BranchId = @BranchId
END

-----
Go
CREATE PROCEDURE sp_GetAllClassBySubBranchId
	@SubBranchId int
AS
 BEGIN 
	SELECT * FROM Class
	WHERE SubBranchId = @SubBranchId
END

-----
Go
CREATE PROCEDURE sp_InsertClass
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@BranchId int,
	@SubBranchId int
AS
 BEGIN
	INSERT INTO Class (LatinName, Name, BranchId, SubBranchId)
	VALUES (@LatinName, @Name, @BranchId, @SubBranchId);
	SELECT Class.Id from Class WHERE @@ROWCOUNT > 0 AND Class.Id = SCOPE_IDENTITY();
END 

-----

GO
CREATE PROCEDURE sp_DeleteClass
	@Id int
AS
 BEGIN
	DELETE FROM Class
	WHERE Id = @Id
END

----

GO
CREATE PROCEDURE sp_UpdateClass
	@Id int,
	@LatinName nvarchar(100),
	@Name nvarchar(100), 
	@BranchId int, 
	@SubBranchId int
AS
 BEGIN
	UPDATE Class
	SET
		LatinName = @LatinName,
		[Name] = @Name,
		BranchId = @BranchId,
		SubBranchId = @SubBranchId
	WHERE Id = @Id
END

-----------------------------
-- ORDER --
-----------------------------
Go
CREATE PROCEDURE sp_GetOrder
	@Id int
AS
 BEGIN
	SELECT * FROM [Order]
	WHERE Id = @Id
END

-----
Go
CREATE PROCEDURE sp_InsertOrder
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@ClassId int
AS
 BEGIN
	INSERT INTO [Order] (LatinName, Name, ClassId)
	VALUES (@LatinName, @Name, @ClassId);
	SELECT [Order].Id from [Order] WHERE @@ROWCOUNT > 0 AND [Order].Id = SCOPE_IDENTITY();
END

-----
Go
CREATE PROCEDURE sp_GetAllOrderByClassId
	@ClassId int
AS
 BEGIN
	SELECT * FROM [Order]
	WHERE ClassId = @ClassId
END

------

-- Last Added
GO
CREATE PROCEDURE sp_DeleteOrder
	@Id int
AS
 BEGIN
	DELETE FROM [Order]
	WHERE Id = @Id
END
------

-- Last Added
GO
CREATE PROCEDURE sp_UpdateOrder
	@Id int,
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@ClassId int
AS
 BEGIN
	UPDATE [Order]
		Set
			LatinName = @LatinName,
			[Name] = @Name,
			ClassId = @ClassId
		WHERE Id = @Id
END

-----------------------------
-- FAMILY --
-----------------------------
Go
CREATE PROCEDURE sp_GetFamily
	@Id int
AS
 BEGIN
	SELECT * FROM Family
	WHERE Id = @Id
END

-----
Go
CREATE PROCEDURE sp_InsertFamily
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@OrderId int
AS
 BEGIN
	INSERT INTO Family (LatinName, [Name], OrderId)
	VALUES (@LatinName, @Name, @OrderId)
	SELECT Family.Id from Family WHERE @@ROWCOUNT > 0 AND Family.Id = SCOPE_IDENTITY();
END

----
Go
CREATE PROCEDURE sp_GetAllFamilyByOrderId
	@OrderId int
AS
 BEGIN
	SELECT * FROM Family
	WHERE OrderId = @OrderId
END

-----

-- Last Added
GO
CREATE PROCEDURE sp_DeleteFamily
	@Id int
AS
 BEGIN
	DELETE FROM Family
	WHERE Id = @Id
END

----

-- Last Added
GO
CREATE PROCEDURE sp_UpdateFamily
	@Id int,
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@OrderId int
AS
 BEGIN
	UPDATE Family
		SET 
			LatinName = @LatinName,
			[Name] = @Name,
			OrderId = @OrderId
	WHERE Id = @Id
END

-----------------------------
-- GENDER --
-----------------------------
Go
CREATE PROCEDURE sp_GetGender
	@Id int
AS
 BEGIN
	SELECT * FROM Gender
	WHERE Id = @Id
END

-----
Go
CREATE PROCEDURE sp_InsertGender
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@FamilyId int
AS
 BEGIN
	INSERT INTO Gender(LatinName, Name, FamilyId)
	VALUES (@LatinName, @Name, @FamilyId)
	SELECT Gender.Id from Gender WHERE @@ROWCOUNT > 0 AND Gender.Id = SCOPE_IDENTITY();
END

-----
Go
CREATE PROCEDURE sp_GetAllGenderByFamilyId
	@FamilyId int
AS
 BEGIN
	SELECT * FROM Gender
	WHERE FamilyId = @FamilyId
END

-----
-- Last Added
GO
CREATE PROCEDURE sp_DeleteGender
	@Id int
AS
 BEGIN
	DELETE FROM Gender
	WHERE Id = @Id
END

-----
-- Last Added
GO
CREATE PROCEDURE sp_UpdateGender
	@Id int,
	@LatinName nvarchar(100),
	@Name nvarchar(100),
	@FamilyId int
AS
 BEGIN
	UPDATE Gender
		SET
			LatinName = @LatinName,
			[Name] = @Name,
			FamilyId = @FamilyId
	WHERE Id = @Id
END

-----------------------------
-- IMG --
-----------------------------
Go
CREATE PROCEDURE sp_GetImg
	@Id int
AS
 BEGIN
	SELECT * FROM Img
	WHERE Id = @Id
END

-----
Go
CREATE PROCEDURE sp_GetImgByObservationId
	@ObservationId int
AS
 BEGIN
	SELECT * FROM Img
	WHERE ObservationId = @ObservationId
END

-----
Go
CREATE PROCEDURE sp_GetAllImgBySpeciesId
	@SpeciesId int
AS
 BEGIN
	SELECT * FROM Img
	WHERE SpeciesId = @SpeciesId
END

-----
Go
CREATE PROCEDURE sp_InsertImg
	@Date date,
	@Data varbinary(Max),
	@Origin nvarchar(50),
	@SpeciesId int,
	@CopyRight nvarchar(150),
	@ObservationId int
AS
 BEGIN
	INSERT INTO Img([Date], Data, Origin, CopyRight, SpeciesId, ObservationId)
	VALUES (@Date, @Data, @Origin, @CopyRight, @SpeciesId, @ObservationId)
	SELECT Img.Id from Img WHERE @@ROWCOUNT > 0 AND Img.Id = SCOPE_IDENTITY();
END

-- 

-- Last Added
GO
CREATE PROCEDURE sp_DeleteImg
	@Id int
AS
 BEGIN
	DELETE FROM Img
	WHERE Id = @Id
END

-- Last Added
CREATE PROCEDURE sp_UpdateImg
	@Id int, 
	@Date date,
	@Data varbinary(Max),
	@Origin nvarchar(50),
	@CopyRight nvarchar(150),
	@SpeciesId int,
	@ObservationId int
AS
BEGIN
UPDATE Img
	Set 
	[Date] = @Date,
	[Data] = @Data,
	Origin = @Origin,
	CopyRight = @CopyRight,
	SpeciesId = @SpeciesId,
	ObservationId = @ObservationId
WHERE Id = @Id;
END

-----------------------------
-- STATUS --
-----------------------------
Go
CREATE PROCEDURE sp_GetStatus
	@Id int
AS
 BEGIN
	SELECT * FROM [Status]
	WHERE Id = @Id
END

-----------------------------
-- OBSERVATION --
-----------------------------
Go
CREATE PROCEDURE sp_InsertObservation
	@DateOfObs date,
	@DateOfPost date,
	@Content nvarchar(200),
	@Longitute decimal(9,6),
	@Latitude decimal(9,6),
	@SpeciesName nvarchar(100),
	@SpeciesId int,
	@StatusId int,
	@ValidatorId int,
	@PublisherId int, 
	@ObserverId int
AS
 BEGIN
	INSERT INTO Observation (DateOfObs, DateOfPost, Content, Longitute, Latitude, SpeciesName, 
		SpeciesId, StatusId, ValidatorId, PublisherId, ObserverId)
	
	VALUES (@DateOfObs, @DateOfPost, @Content, @Longitute, @Latitude, @SpeciesName, 
		@SpeciesId, @StatusId, @ValidatorId, @PublisherId, @ObserverId);
	SELECT Img.Id from Img WHERE @@ROWCOUNT > 0 AND Img.Id = SCOPE_IDENTITY();
END

-----
Go
CREATE PROCEDURE sp_GetObservation
	@Id int
AS
 BEGIN
	SELECT * FROM Observation
	WHERE Id = @Id
END

-----
Go
CREATE PROCEDURE sp_GetAllObservationByObserverId
	@ObserverId int
AS
 BEGIN
	SELECT * FROM Observation
	WHERE ObserverId = ObserverId
END

-----

-- Last Added
GO
CREATE PROCEDURE sp_GetAllObservationByPublisherId
	@PublisherId int
AS
 BEGIN
	SELECT * FROM Observation
	WHERE PublisherId = @PublisherId
END

-----

-- Last Added
GO
CREATE PROCEDURE sp_GetAllObservationByValidatorId
	@ValidatorId int
AS
 BEGIN
	SELECT * FROM Observation
	WHERE ValidatorId = @ValidatorId
END

-----

-- Last Added
GO
CREATE PROCEDURE sp_GetAllObservationByStatusId
	@StatusId int
AS
 BEGIN
	SELECT * FROM Observation
	WHERE StatusId = @StatusId
END

-----

Go
CREATE PROCEDURE sp_UpdateObservation
	@Id int,
	@DateOfObs date,
	@DateOfPost date,
	@Content nvarchar(200),
	@Longitute decimal(9,6),
	@Latitude decimal(9,6),
	@SpeciesName nvarchar(100),
	@SpeciesId int,
	@StatusId int,
	@ValidatorId int,
	@PublisherId int, 
	@ObserverId int
AS
 BEGIN	
	UPDATE Observation
	 SET 
		 DateOfObs = @DateOfObs,
		 DateOfPost = @DateOfPost,
		 Content = @Content,
		 Longitute = @Longitute,
	     Latitude = @Latitude,
		 SpeciesName = @SpeciesName,
	     SpeciesId = @SpeciesId,
		 StatusId = @StatusId,
		 ValidatorId = @ValidatorId,
		 PublisherId = @PublisherId,
		 @ObserverId = @ObserverId
	WHERE Id = @Id
 END
