Create database Playzone_DB_Env_v10;

Use Playzone_DB_Env_v10;

Go
Create Table [Role](
	Id int Primary Key Identity,
	[Type] nvarchar(100) not null,
	Constraint UK_Role_Type Unique (Type)
);

insert into [Role] values ('SuperAdmin')
insert into [Role] values ('Admin')
insert into [Role] values ('Assistant')

Go
Create Table [Status](
	Id int Primary Key Identity,
	[Type] nvarchar(100) not null,
	Constraint UK_Status_Type Unique (Type)
);

insert into [Status] (Type) values ('Aucun')
insert into [Status] (Type) values ('Brouillon')
insert into [Status] (Type) values ('Valid�e')
insert into [Status] (Type) values ('Publi�e')
insert into [Status] (Type) values ('Supprim�e')
select * from Status

Go
Create Table [User](
	Id int Primary Key Identity,
	[Name] nvarchar(100) not null,
	LastName nvarchar(100) not null,
	Email nvarchar(150) not null,
	[Login] nvarchar(100) not null,
	Pwd varbinary(8000) not null,
	Active bit default 1,
	RoleId int not null,
	Constraint FK_User_Role Foreign Key (RoleId) References [Role](Id),
	Constraint UK_User_Email Unique (Email)
);

--insert into [User] values ('Honnay', 'Elise', 'elise.honnay@province.namur.be', 'elise', 'test1234=', 1, 1)
--insert into [User] values ('Duroy', 'Fran�ois', 'f.duroy@province.namur.be', 'franz', 'test1234=', 1, 2)

Go
Create Table Kingdom (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100) not null,
	Name nvarchar(100),
	Constraint UK_Kingdom_LatinName Unique (LatinName),
);

insert into Kingdom Values ('Ind�fini', 'Ind�fini');

Go
Create Table Branch (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100) not null,
	Name nvarchar(100),
	KingdomId int not null,
	Constraint FK_Branch_Kingdom Foreign Key (KingdomId) references Kingdom (Id),
	Constraint UK_Branch_LatinName Unique (LatinName),
); 

insert into Branch values ('Ind�fini', 'Ind�fini', 1)

Go
Create Table SubBranch (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100) not null,
	Name nvarchar(100),
	BranchId int not null,
	Constraint FK_SubBranch_Branch Foreign Key (BranchId) references Branch (Id),
	Constraint UK_SubBranch_LatinName Unique (LatinName),
);

insert into SubBranch values ('Ind�fini', 'Ind�fini', 1);

Go
Create Table Class (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100) not null,
	Name nvarchar(100),
	BranchId int not null,
	SubBranchId int not null default 1,
	Constraint FK_Class_Branch Foreign Key (BranchId) references Branch (Id),
	Constraint FK_Class_SubBranch Foreign Key (SubBranchId) references SubBranch (Id),
	Constraint UK_Class_LatinName Unique (LatinName),
	-- Trigger : un des deux FK (BranchId ou SubBranchId) doit �tre rempli.. pas les deux, ni aucun, un seul des deux champs
	-- Trigger : si on delete une sous-branche, d''abord deleter les classes correspondantes
);

insert into Class Values ('Ind�fini', 'Ind�fini', 1, 1)

Go
Create Table [Order] (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100) not null,
	Name nvarchar(100),
	ClassId int not null,
	Constraint FK_Order_Class Foreign Key (ClassId) references Class (Id),
	Constraint UK_Order_LatinName Unique (LatinName),
);

insert into [Order] values ('Ind�fini', 'Ind�fini', 1)

Go
Create Table Family (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100) not null,
	Name nvarchar(100),
	OrderId int not null,
	Constraint FK_Family_Order Foreign Key (OrderId) references [Order] (Id),
	Constraint UK_Family_LatinName Unique (LatinName),
);

insert into Family values ('Ind�fini', 'Ind�fini', 1)

Go
Create Table Gender (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100) not null,
	Name nvarchar(100),
	FamilyId int not null,
	Constraint FK_Gender_Family Foreign Key (FamilyId) references Family (Id),
	Constraint UK_Gender_LatinName Unique (LatinName),
);

insert into Gender values ('Ind�fini', 'Ind�fini', 1)

Go
Create Table Species (
	Id int Primary Key Identity not null,
	LatinName nvarchar(100),
	Name nvarchar(100),
	[Description] nvarchar (1000),
	PeriodBegin int not null,
	PeriodEnd int not null,
	PublisherId int not null default 1,
	GenderId int not null,
	StatusId int not null default 1,
	Constraint FK_Species_User Foreign Key (PublisherId) References [User] (Id),
	Constraint FK_Species_Gender Foreign Key (GenderId) References Gender (Id),
	Constraint FK_Species_Status Foreign Key (StatusId) References [Status] (Id),
	Constraint UK_Species_LatinName Unique (LatinName),
	Constraint CK_PeriodBeginVal Check (PeriodBegin between 1 and 12),
	Constraint CK_PeriodEndVal Check (PeriodEnd between 1 and 12)
	-- trigger : le status d'une esp�ce a pour valeur : 1 (Aucun), 2 (Brouillon), 4 (Publi�e), 5 (Supprim�e)
	-- trigger : le publisher d'une esp�ce a pour valeur : 1 (SuperAdmin), 2 (Admin)
	-- NB : si le publisher est null, seul le SuperAdmin voit la fiche (et peut l'attribuer � quelqu'un pour traitement)
);

insert into Species values ('Inconnue', 'Inconnue', ' ', 1, 12, 1, 1, 1)

Go
Create Table Observer (
	Id int Primary Key Identity,
	MachineId nvarchar(150) not null default '00000000-54b3-e7c7-0000-000046bffd97',
	--Name nvarchar(150) null,
	--Email nvarchar(150) null,
	Banned bit not null default 0
	-- Trigger : supprimer les images puis les observations si le user est banni
	-- Trigger : virer l'email adresse de la DB quand l'email aura �t� envoy� � l'utilisateur
	-- MachineId : adresse IP du PC ou ID du t�l�phone pour pouvoir exclure qqun qui fait des observations 'non utiles'
	-- MachineId : virer le d�fault quand les tests sont faits
);

Go
Create Table Observation(
	Id int Primary Key Identity,
	DateOfPost date not null default GetDate(), 
	DateOfObs date not null default GetDate(),
	Content nvarchar(200),
	Longitute decimal(9,6) not null,
	Latitude decimal(9,6) not null,
	SpeciesName nvarchar(100),
	SpeciesId int not null default 1,
	StatusId int default 1,
	ValidatorId int not null default 1,
	PublisherId int default 1,
	ObserverId int not null,
	Constraint FK_Observation_Species Foreign Key (SpeciesId) References Species (Id),
	Constraint FK_Observation_User_Validator Foreign Key (ValidatorId) References [User] (Id),
	Constraint FK_Observation_User_Publisher Foreign Key (PublisherId) References [User] (Id),
	Constraint FK_Observation_Observer Foreign Key (ObserverId) References Observer(Id),
	Constraint FK_Observation_Status Foreign Key (StatusId) References [Status] (Id),
	Constraint CK_DateOfPost Check (DateOfPost <= GetDate()), 
	Constraint CK_DateOfObs Check (DateOfObs <= GetDate()),
	-- NB : le status d'une observation a pour valeur : 1 (Aucun), 2 (Brouillon), 3 (Valid�e), 4 (Publi�e), 5 (Supprim�e) 
	-- Trigger : si le validatorId est diff�rent de null, le statut passe � 2 (brouillon)
	-- Trigger : si le status est 3 (Valid�e), le publisherId devient 1 (SuperAdmin), lui devrai faire la v�rification ou d�l�guer � un Admin (2)
	-- Trigger : le publisherId est 1 (SuperAdmin) ou 2 (Admin)
); 

Go
Create Table Img(
	Id int Primary Key Identity,
	[Date] date not null default GetDate(),
	[Data] varbinary(Max) not null, 
	Origin nvarchar(50),
	CopyRight nvarchar(150),
	SpeciesId int,
	ObservationId int,
	Constraint FK_Img_Species Foreign Key (SpeciesId) References Species (Id),
	Constraint FK_Img_Observation Foreign Key (ObservationId) References Observation (Id),
	Constraint UK_Img_Observation Unique (ObservationId)
);

-- Kingdoms --
insert into Kingdom values			('Fungi',				'Champignons Et Lichens');
insert into Kingdom values			('Animalia',			'Animaux');
insert into Kingdom values			('Plantae',				'Plantes');
select * from Kingdom;

-- Branches --
insert into Branch values			('Basidiomycota',		'Basidiomyc�tes',			2);
insert into Branch values			('Chordata',			'Chord�s',					3);
insert into Branch values			('Tracheophyta',		'Plantes Vasculaires',		4);
insert into Branch values			('Marchantiophyta',		'H�patiques',				4);
select * from Branch;

-- SubBranches --
insert into SubBranch values		('Vertebrata',			'Vert�br�s',				3); 
insert into SubBranch values		('Angiospermae',		'Plante � Fleurs',			4); 
select * from SubBranch;

-- Classes -- 
insert into Class values			('Agaricomycetes',		null,						2,	1);
insert into Class values			('Amphibia',			'Amphibiens',				3,	2);
insert into Class values			('Mammalia',			'Mammif�res',				3,	2);
insert into Class values			('Liliopsida',			'Monocotyl�done',			4,	4);
insert into Class values			('Jungermanniopsida',	null,						5,	1);

select * from Class;

-- Orders -- 
insert into [Order] values			('Agaricales',			null,						4);
insert into [Order] values			('Anura',				'Anoures',					3); 
insert into [Order] values			('Lagomorpha',			'Lagomorphes',				4); 
insert into [Order] values			('Liliales',			null,						7); 
insert into [Order] values			('Jungermanniales',		null,						8); 
insert into [Order] values			('Russulales',			null,						4); 

select * from [Order];

-- Families -- 
insert into Family values			('Amanitaceae',			null,						2);
insert into Family values			('Ranidae',				null,						3);
insert into Family values			('Leporidae',			'L�porid�s',				8);
insert into Family values			('Melanthiaceae',		null,						9);
insert into Family values			('Scapaniaceae',		null,						11);
insert into Family values			('Russulaceae',			null,						7);

select * from Family;

-- Genders -- 
insert into Gender values			('Amanita',				'Amanites',					2);
insert into Gender values			('Rana',				'Grenouilles Brunes',		3);
insert into Gender values			('Lepus',				'Li�vres',					8);
insert into Gender values			('Paris',				null,						10);
insert into Gender values			('Diplophyllum',		null,						11);
insert into Gender values			('Russula',				'Russules',					7);

select * from Gender;

-- Species -- 
--insert into Species values			('Amanita muscaria',		'Amanite Tue-Mouches',					'Description', 9, 4, 1, 2, 4)
--insert into Species values			('Rana temporaria',			'Grenouille Rousse',					'Description', 1, 12, 1, 3, 4)
--insert into Species values			('Lepus europaeus',			'Li�vre d''Europe',						'Description', 1, 12, 1, 4, 4)
--insert into Species values			('Paris quadrifolia',		'Parisette � Quatre Feuilles',			'Description', 1, 12, 1, 5, 4)
--insert into Species values			('Diplophyllum albicans',	null,									'Description', 1, 12, 1, 6, 4)
select * from v_Species where Id <> 1;

-- Observer -- 
Insert into Observer (MachineId) Values (default);
select * from Observer;


-- Observation -- 
--Insert into Observation (DateOfObs, DateOfPost, Content, Longitute, Latitude, SpeciesName, SpeciesId, StatusId, ValidatorId, PublisherId, ObserverId)
-- values (default, default, 'J''ai vu un stron', -78.948237, 35.929673, 'Amanita muscaria', 1, 1, 1, 1, 1)
--select * from Observation;

--Insert into Observation (DateOfObs, DateOfPost, Content, Longitute, Latitude, SpeciesName, SpeciesId, StatusId, ValidatorId, PublisherId, ObserverId)
-- values (default, default, 'J''ai vu un stron rouge', -78.948237, 35.929673, 'Rana temporaria', 2, 1, 2, 1, 1)


