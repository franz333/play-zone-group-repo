USE [Playzone_DB_Env_v10]
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertSpecies]    Script Date: 17-12-19 08:44:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_InsertSpecies]
	@Description nvarchar(1000),
	@GenderId int,
	@LatinName nvarchar(100), 
	@Name nvarchar(100), 
	@PeriodBegin int, 
	@PeriodEnd int, 
	@PublisherId int, 
	@StatusId int
AS
 BEGIN
	INSERT INTO Species ([Description], GenderId, LatinName, Name, PeriodBegin, PeriodEnd, PublisherId, StatusId)	
	VALUES (@Description, @GenderId, @LatinName, @Name, @PeriodBegin, @PeriodEnd, @PublisherId, @StatusId);
	--SELECT Species.Id from Species WHERE @@ROWCOUNT > 0 AND Species.Id = SCOPE_IDENTITY();
END

------
