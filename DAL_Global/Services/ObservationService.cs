﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class ObservationService : IRepository<Observation>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertObservation", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteNonQuery(cmd) > 0;
		}

		public Observation Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetObservation", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observation>).SingleOrDefault();
		}

		public IEnumerable<Observation> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("select * from v_Observation", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observation>);
		}

		public IEnumerable<Observation> GetAllByObserverId(int observerId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllObservationByObserverId", true);
			cmd.AddParameter("@ObserverId", observerId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observation>);
		}

        public IEnumerable<Observation> GetAllByValidatorId(int validatorId)
        {
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_GetAllObservationByValidatorId", true);
            cmd.AddParameter("@ObserverId", validatorId);

            return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observation>);
        }

        public IEnumerable<Observation> GetAllByPublisherId(int publisherId)
        {
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_GetAllObservationByPublisherId", true);
            cmd.AddParameter("@PublisherId", publisherId);

            return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observation>);
        }

        public IEnumerable<Observation> GetAllByStatusId(int statusId)
        {
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_GetAllObservationByStatusId", true);
            cmd.AddParameter("@StatusId", statusId);

            return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observation>);
        }

        public int Insert(Observation entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertObservation", true);
			cmd.AddParameter("@DateOfObs", entity.DateOfObs);
			cmd.AddParameter("@DateOfPost", entity.DateOfPost);
			cmd.AddParameter("@Content", entity.Content);
			cmd.AddParameter("@Longitute", entity.Longitude);
			cmd.AddParameter("@Latitude", entity.Latitude);
			cmd.AddParameter("@SpeciesName", entity.SpeciesName);
			cmd.AddParameter("@SpeciesId", entity.SpeciesId);
			cmd.AddParameter("@StatusId", entity.StatusId);
			cmd.AddParameter("@ValidatorId", entity.ValidatorId);
			cmd.AddParameter("@PublisherId", entity.PublisherId);
			cmd.AddParameter("@ObserverId", entity.ObserverId);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Observation entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_UpdateObservation", true);
            cmd.AddParameter("@DateOfObs", entity.DateOfObs);
            cmd.AddParameter("@DateOfPost", entity.DateOfPost);
            cmd.AddParameter("@Content", entity.Content);
            cmd.AddParameter("@Longitute", entity.Longitude);
            cmd.AddParameter("@Latitude", entity.Latitude);
            cmd.AddParameter("@SpeciesName", entity.SpeciesName);
            cmd.AddParameter("@SpeciesId", entity.SpeciesId);
            cmd.AddParameter("@StatusId", entity.StatusId);
            cmd.AddParameter("@ValidatorId", entity.ValidatorId);
            cmd.AddParameter("@PublisherId", entity.PublisherId);
            cmd.AddParameter("@ObserverId", entity.ObserverId);
            cmd.AddParameter("@Id", entity.Id);

            return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
