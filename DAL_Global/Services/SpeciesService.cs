﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
    public class SpeciesService : IRepository<Species>
    {
        public bool Delete(int key)
        {
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteSpecies", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
        }

        public Species Get(int key)
        {
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("Select * from Species where Id = @Id", false);
            cmd.AddParameter("@Id", key);

            return con.ExecuteReader(cmd, UniversalMapper.Mapper<Species>).SingleOrDefault();
        }


		public IEnumerable<Species> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from Species", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Species>);
		}

        // TODO
        //public IEnumerable<Species> GetAllSpeciesByMonth(int month)
        //{
        //    Connection con = new Connection(DBConfig.ConnectionString);
        //    Command cmd = new Command("", true);
        //    cmd.AddParameter("@Month", month);

        //    return con.ExecuteReader(cmd, UniversalMapper.Mapper<Species>);
        //}

        public int Insert(Species entity)
        {
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_InsertSpecies", true);

            cmd.AddParameter("@StatusId", entity.StatusId);
            cmd.AddParameter("@Description", entity.Description);
            cmd.AddParameter("@GenderId", entity.GenderId);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@PeriodBegin", entity.PeriodBegin);
            cmd.AddParameter("@PeriodEnd", entity.PeriodEnd);
            cmd.AddParameter("@PublisherId", entity.PublisherId);            

            return (int)con.ExecuteScalar(cmd);
        }

        public bool Update(Species entity)
        {
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_UpdateSpecies", true);

			cmd.AddParameter("@Id", entity.Id);
			cmd.AddParameter("@StatusId", entity.StatusId);
			cmd.AddParameter("@Description", entity.Description);
			cmd.AddParameter("@GenderId", entity.GenderId);
			cmd.AddParameter("@LatinName", entity.LatinName);
			cmd.AddParameter("@Name", entity.Name);
			cmd.AddParameter("@PeriodBegin", entity.PeriodBegin);
			cmd.AddParameter("@PeriodEnd", entity.PeriodEnd);
			cmd.AddParameter("@PublisherId", entity.PublisherId);

			return con.ExecuteNonQuery(cmd) > 0;
        }

		#region DTO METHODS

		public SpeciesDTO GetDTO(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from v_Species where Id = @Id", false);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>).SingleOrDefault();
		}

		public IEnumerable<SpeciesDTO> GetAllDTO()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select top 20 * from v_Species", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllByStatusId(int statusId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByStatusId", true);
			cmd.AddParameter("@StatusId", statusId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllByPublisherId(int publisherId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByPublisherId", true);
			cmd.AddParameter("@PublisherId", publisherId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesByPeriod(int periodBegin, int periodEnd)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByPeriod", true);
			cmd.AddParameter("@PeriodBegin", periodBegin);
			cmd.AddParameter("@PeriodEnd", periodEnd);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesByKingdomId(int kingdomId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByKingdomId", true);
			cmd.AddParameter("@KingdomId", kingdomId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesByBranchId(int branchId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByBranchId", true);
			cmd.AddParameter("@BranchId", branchId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesBySubBranchId(int subBranchId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesBySubBranchId", true);
			cmd.AddParameter("@SubBranchId", subBranchId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesByOrderId(int orderId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByOrderId", true);
			cmd.AddParameter("@OrderId", orderId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesByFamilyId(int familyId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByFamilyId", true);
			cmd.AddParameter("@FamilyId", familyId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesByGenderId(int genderId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByGenderId", true);
			cmd.AddParameter("@GenderId", genderId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}

		public IEnumerable<SpeciesDTO> GetAllSpeciesByClassId(int classId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSpeciesByClassId", true);
			cmd.AddParameter("@ClassId", classId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SpeciesDTO>);
		}
		#endregion
	}
}
