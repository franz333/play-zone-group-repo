﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class ImgService : IRepository<Img>
	{
		public bool Delete(int key)
		{
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteImg", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public Img Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetImg", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Img>).SingleOrDefault();
		}

		public Img GetImgByObservationId(int observationId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetImgByObservationId", true);
			cmd.AddParameter("@ObservationId", observationId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Img>).SingleOrDefault();
		}

		public IEnumerable<Img> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from Img", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Img>);
		}

		public IEnumerable<Img> GetAllImgBySpeciesId(int speciesId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllImgBySpeciesId", true);
            cmd.AddParameter("@SpeciesId", speciesId);

            return con.ExecuteReader(cmd, UniversalMapper.Mapper<Img>);
		}

		public int Insert(Img entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertImg", true);
			cmd.AddParameter("@Date", entity.Date);
			cmd.AddParameter("@Data", entity.Data, DbType.Binary);
			cmd.AddParameter("@CopyRight", entity.CopyRight);
			cmd.AddParameter("@Origin", entity.Origin);
			cmd.AddParameter("@SpeciesId", entity.SpeciesId);
			cmd.AddParameter("@ObservationId", entity.ObservationId);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Img entity)
		{
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateImg", true);
            cmd.AddParameter("@Date", entity.Date);
            cmd.AddParameter("@Data", entity.Data, DbType.Binary);
            cmd.AddParameter("@Origin", entity.Origin);
            cmd.AddParameter("@CopyRight", entity.CopyRight);
            cmd.AddParameter("@SpeciesId", entity.SpeciesId);
            cmd.AddParameter("@ObservationId", entity.ObservationId);
            cmd.AddParameter("@Id", entity.Id);

            return con.ExecuteNonQuery(cmd) > 0;
        }
	}
}
