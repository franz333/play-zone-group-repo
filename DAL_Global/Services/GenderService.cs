﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class GenderService : IRepository<Gender>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteGender", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public Gender Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetGender", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Gender>).SingleOrDefault();
		}

		public IEnumerable<Gender> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from Gender", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Gender>); 
		}
		
		public IEnumerable<Gender> GetAllGenderByFamilyId(int familyId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllGenderByFamilyId", true);
			cmd.AddParameter("@FamilyId", familyId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Gender>);
		}

		public int Insert(Gender entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertGender", true);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@FamilyId", entity.FamilyId);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Gender entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateGender", true);
            cmd.AddParameter("@Id", entity.Id);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@FamilyId", entity.FamilyId);

            return con.ExecuteNonQuery(cmd) > 0;

        }
	}
}
