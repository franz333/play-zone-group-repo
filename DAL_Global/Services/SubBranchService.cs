﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class SubBranchService : IRepository<SubBranch>
	{
		public bool Delete(int key)
		{
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteSubBranch", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public SubBranch Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetSubBranch", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SubBranch>).SingleOrDefault();
		}

		public IEnumerable<SubBranch> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("select * from SubBranch", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SubBranch>);
		}

		public IEnumerable<SubBranch> GetAllSubBranchByBranchId(int branchId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllSubBranchByBranchId", true);
			cmd.AddParameter("@BranchId", branchId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<SubBranch>);
		}		

		public int Insert(SubBranch entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertSubBranc", true);
			cmd.AddParameter("@LatinName", entity.LatinName); 
			cmd.AddParameter("@Name", entity.Name);
			cmd.AddParameter("@BranchId", entity.BranchId);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(SubBranch entity)
		{
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateSubBranch", true);
            cmd.AddParameter("@Id", entity.Id);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@BranchId", entity.BranchId);

            return con.ExecuteNonQuery(cmd) > 0;
        }
	}
}
