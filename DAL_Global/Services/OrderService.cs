﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class OrderService : IRepository<Order>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteOrder", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public Order Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetOrder", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Order>).SingleOrDefault();
		}

		public IEnumerable<Order> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from [Order]", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Order>);
		}

		public IEnumerable<Order> GetAllOrderByClassId(int classId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllOrderByClassId", true);
			cmd.AddParameter("@ClassId", classId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Order>);
		}		

		public int Insert(Order entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertOrder", true);
			cmd.AddParameter("@LatinName", entity.LatinName);
			cmd.AddParameter("@Name", entity.Name);
			cmd.AddParameter("@ClassId", entity.ClassId);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Order entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateOrder", true);
            cmd.AddParameter("@Id", entity.Id);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@ClassId", entity.ClassId);

            return con.ExecuteNonQuery(cmd) > 0;
        }
	}
}
