﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class ClassService : IRepository<Class>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteClass", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public Class Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetClass", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Class>).SingleOrDefault();
		}

		public IEnumerable<Class> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("select * from Class", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Class>); 
		}

		public IEnumerable<Class> GetAllClassByBranchId(int branchId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllClassByBranchId", true);
			cmd.AddParameter("@BranchId", branchId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Class>);
		}

		public IEnumerable<Class> GetAllClassBySubBranchId(int subBranchId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllClassBySubBranchId", true);
			cmd.AddParameter("@SubBranchId", subBranchId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Class>);
		}

		public int Insert(Class entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertClass", true);
			cmd.AddParameter("@LatinName", entity.LatinName);
			cmd.AddParameter("@Name", entity.Name);
			cmd.AddParameter("@BranchId", entity.BranchId);
			cmd.AddParameter("@SubBranchId", entity.SubBranchId);

			return (int)con.ExecuteScalar(cmd);

		}

		public bool Update(Class entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateClass", true);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@BranchId", entity.BranchId);
            cmd.AddParameter("@SubBranchId", entity.SubBranchId);
            cmd.AddParameter("@Id", entity.Id);

            return con.ExecuteNonQuery(cmd) > 0;
        }
	}
}
