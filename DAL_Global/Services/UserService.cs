﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class UserService : IRepository<User>
	{		
		public bool Delete(int id)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_DeleteUser", true);
			cmd.AddParameter("@id", id);
			return con.ExecuteNonQuery(cmd) > 0;
		}

		public User Get(int id)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from v_User where Id = @id", false);
			cmd.AddParameter("@id", id);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<User>).FirstOrDefault();
		}

		public User Login(User user)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_LoginUser", true);
			cmd.AddParameter("@Login", user.Login);
			cmd.AddParameter("@Pwd", user.Pwd);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<User>).SingleOrDefault();
		}

		public IEnumerable<User> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from v_User", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<User>);
		}

		public int Insert(User entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertUser", true);
			cmd.AddParameter("@Name", entity.Name);
			cmd.AddParameter("@LastName", entity.LastName);
			cmd.AddParameter("@Email", entity.Email);
			cmd.AddParameter("@Login", entity.Login);
			cmd.AddParameter("@Pwd", entity.Pwd, DbType.Binary);
			cmd.AddParameter("@Active", entity.Active);
			cmd.AddParameter("@RoleId", entity.RoleId);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(User entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_UpdateUser", true);
			cmd.AddParameter("@Id", entity.Id);
			cmd.AddParameter("@Name", entity.Name);
			cmd.AddParameter("@LastName", entity.LastName);
			cmd.AddParameter("@Email", entity.Email);
			cmd.AddParameter("@Login", entity.Login);
			cmd.AddParameter("@Pwd", entity.Pwd, DbType.Binary);
			cmd.AddParameter("@Active", entity.Active);
			cmd.AddParameter("@RoleId", entity.RoleId);

			return con.ExecuteNonQuery(cmd) > 0;
		}

		public bool Deactivate(int id) 
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_DeactivateUser", true);
			cmd.AddParameter("@Id", id);

			return con.ExecuteNonQuery(cmd) > 0;
		}

        public bool Activate(int id)
        {
            Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_ActivateUser", true);
            cmd.AddParameter("@id", id);

            return con.ExecuteNonQuery(cmd) > 0;
        }
	}
}
