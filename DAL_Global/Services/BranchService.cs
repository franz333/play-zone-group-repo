﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class BranchService : IRepository<Branch>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteBranch", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public Branch Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetBranch", true);
            cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Branch>).SingleOrDefault();
		}

		public IEnumerable<Branch> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("select * from Branch", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Branch>);
		}

		public IEnumerable<Branch> GetAllBranchByKingdomId(int kingdomId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllBranchByKingdomId", true);
			cmd.AddParameter("@kingdomId", kingdomId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Branch>);
		}

		public int Insert(Branch entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_InsertBranch", true);

            return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Branch entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateBranch", true);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@KingdomId", entity.KingdomId);
            cmd.AddParameter("@Id", entity.Id);

            return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
