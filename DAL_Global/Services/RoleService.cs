﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class RoleService : IRepository<Role>
	{
		// Géré en SQL si besoin, mais cela ne devrait pas être nécessaire (roles fixés au départ)
		public bool Delete(int key)
		{
			throw new NotImplementedException();
		}

		public Role Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetRole", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Role>).SingleOrDefault();
		}

		public IEnumerable<Role> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("Select * from Role", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Role>);
		}

		// Géré en SQL à la création de la DB (roles fixés au départ)
		public int Insert(Role entity)
		{
			throw new NotImplementedException();
		}

		// Géré en SQL si besoin, mais cela ne devrait pas être nécessaire (roles fixés au départ)
		public bool Update(Role entity)
		{
			throw new NotImplementedException();
		}
	}
}
