﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class KingdomService : IRepository<Kingdom>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteKingdom", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public Kingdom Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetKingdom", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Kingdom>).SingleOrDefault();

		}

		public IEnumerable<Kingdom> GetAll()
		{
			//TODO : view or stored procedure
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("select * from Kingdom where Id <> 1", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Kingdom>);
		}

		public int Insert(Kingdom entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertKingdom", true);
			cmd.AddParameter("@LatinName", entity.LatinName);
			cmd.AddParameter("@Name", entity.Name);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Kingdom entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateKingdom", true);
            cmd.AddParameter("@Id", entity.Id);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);

            return con.ExecuteNonQuery(cmd) > 0;
        }
	}
}
