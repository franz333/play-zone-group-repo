﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class ObserverService : IRepository<Observer>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("", true);

			return con.ExecuteNonQuery(cmd) > 0;
		}

		public Observer Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("", true);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observer>).SingleOrDefault();
			
		}

		public IEnumerable<Observer> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("", true);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Observer>);
		}

		public int Insert(Observer entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("", true);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Observer entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("", true);

			return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
