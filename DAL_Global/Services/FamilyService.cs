﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class FamilyService : IRepository<Family>
	{
		public bool Delete(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_DeleteFamily", true);
            cmd.AddParameter("@Id", key);

            return con.ExecuteNonQuery(cmd) > 0;
		}

		public Family Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetFamily", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Family>).SingleOrDefault();
		}

		public IEnumerable<Family> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("select * from Family", false);
			
			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Family>);
		}

		public IEnumerable<Family> GetAllFamilyByOrderId(int orderId)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetAllFamilyByOrderId", true);
			cmd.AddParameter("@OrderId", orderId);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Family>);
		}

		public int Insert(Family entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_InsertFamily", true);
			cmd.AddParameter("@LatinName", entity.LatinName);
			cmd.AddParameter("@Name", entity.Name);
			cmd.AddParameter("@OrderId", entity.OrderId);

			return (int)con.ExecuteScalar(cmd);
		}

		public bool Update(Family entity)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
            Command cmd = new Command("sp_UpdateFamily", true);
            cmd.AddParameter("@Id", entity.Id);
            cmd.AddParameter("@LatinName", entity.LatinName);
            cmd.AddParameter("@Name", entity.Name);
            cmd.AddParameter("@OrderId", entity.OrderId);

            return con.ExecuteNonQuery(cmd) > 0;
		}
	}
}
