﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Mapper;
using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Services
{
	public class StatusService : IRepository<Status>
	{
		public bool Delete(int key)
		{
			throw new NotImplementedException();
		}

		public Status Get(int key)
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("sp_GetStatus", true);
			cmd.AddParameter("@Id", key);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Status>).SingleOrDefault();
		}

		public IEnumerable<Status> GetAll()
		{
			Connection con = new Connection(DBConfig.ConnectionString);
			Command cmd = new Command("select * from [Status]", false);

			return con.ExecuteReader(cmd, UniversalMapper.Mapper<Status>);
		}

		public int Insert(Status entity)
		{
			throw new NotImplementedException();
		}

		public bool Update(Status entity)
		{
			throw new NotImplementedException();
		}
	}
}
