﻿using DAL_Global.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Entities
{
	public class User
	{
		[Key]
		public int Id { get; set; }

		public string Name { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

        private string _login;

        public string Login
        {
            get
            {
                return _login.ToUpperFirstCaractereAllMots();
            }
            set { _login = value; }
        }

        public byte[] Pwd { get; set; }

		public bool Active { get; set; }

		public string RoleType { get; set; }

		public int RoleId { get; set; }
	}
}
