﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Entities
{
	public class Species : Kingdom
	{
		public string Description { get; set; }

		public int PeriodBegin { get; set; }

		public int PeriodEnd { get; set; }

		public int StatusId { get; set; }

		public int PublisherId { get; set; }

		public int GenderId { get; set; }		
	}
}
