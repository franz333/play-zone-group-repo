﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Entities
{
	public class Img
	{
		[Key]
		public int Id { get; set; }
		public DateTime Date { get; set; }
		public byte[] Data { get; set; }
		public string Origin { get; set; }
		public string CopyRight { get; set; }
		public int? SpeciesId { get; set; }
		public int? ObservationId { get; set; }
        public string Format { get; set; }
	}
}
