﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Entities
{
	public class SpeciesDTO : Kingdom
	{
		public string Description { get; set; }

		public int PeriodBegin { get; set; }

		public int PeriodEnd { get; set; }

		public int StatusId { get; set; }

		public string StatusType { get; set; }

		public int PublisherId { get; set; }

		public string PublisherName { get; set; }

		public string PublisherLastName { get; set; }

		public int SubBranchId { get; set; }

		public string SubBranchLatinName { get; set; }

		public string SubBranchName { get; set; }

		public int GenderId { get; set; }

		public string GenderLatinName { get; set; }

		public string GenderName { get; set; }

		public int FamilyId { get; set; }

		public string FamilyLatinName { get; set; }

		public string FamilyName { get; set; }

		public int OrderId { get; set; }

		public string OrderLatinName { get; set; }

		public string OrderName { get; set; }

		public int ClassId { get; set; }

		public string ClassLatinName { get; set; }

		public string ClassName { get; set; }

		public int BranchId { get; set; }

		public string BranchLatinName { get; set; }

		public string BranchName { get; set; }

		public int KingdomId { get; set; }

		public string KingdomLatinName { get; set; }

		public string KingdomName { get; set; }
	}
}
