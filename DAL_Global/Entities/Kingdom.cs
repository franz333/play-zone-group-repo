﻿using DAL_Global.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Entities
{
	public class Kingdom 
	{
		[Key]
		public int Id { get; set; }

        private string _latinName;

        public string LatinName
        {
            get 
            { 
                return _latinName.ToUpperFirstCaractereAllMots(); 
            }
            set { _latinName = value; }
        }
               
        public string Name { get; set; }
	}
}
