﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Entities
{
	public class Status 
	{
		public int Id { get; set; }

		public string Type { get; set; }
	}
}
