﻿using DB_Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Interfaces
{
	internal interface IRepository<TEntity>
	{
		TEntity Get(int key);

		IEnumerable<TEntity> GetAll();

		int Insert(TEntity entity);

		bool Update(TEntity entity);

		bool Delete(int key);
	}
}
