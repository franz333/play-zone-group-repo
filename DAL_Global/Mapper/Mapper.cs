﻿using DAL_Global.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Mapper
{
	internal static class Mapper
	{
		internal static User ToUser(IDataRecord record)
		{
			return new User()
			{
				Active = (bool)record["Active"],
				Email = record["Email"].ToString(),
				Id = (int)record["Id"],
				LastName = record["LastName"].ToString(),
				Login = record["Login"].ToString(),
				Name = record["Name"].ToString(),
				Pwd = record["Pwd"].ToString(),
				RoleId = (int)record["RoleId"],
				RoleType = record["RoleType"].ToString()
			};
		}

		internal static Species ToSpecies(IDataRecord record)
		{
			return new Species()
			{
				Active = (bool)record["Active"],
				Deleted = (bool)record["Deleted"],
				Description = record["Description"].ToString(),
				GenderId = (int)record["GenderId"],
				LatinName = record["LatinName"].ToString(),
				Name = record["Name"].ToString(),
				PeriodBegin = (int)record["PeriodBegin"],
				PeriodEnd = (int)record["PerdiodEnd"],
				PublisherId = (int)record["PublisherId"],
			};
		}
	}
}
