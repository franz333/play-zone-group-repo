﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Global.Tools
{
    public static class Caractere
    {
        public static string ToUpperFirstCaractere(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }
            else
            {
                string ret;
                ret = str.Substring(0, 1).ToUpper() + str.Substring(1, str.Length - 1).ToLower();
                return ret;
            }
        }

        public static string ToUpperFirstCaractereAllMots(this string str)
        {
            string ret = "";

            if (string.IsNullOrWhiteSpace(str))
            {
                return str;
            }
            else
            {
                string[] b = str.Split(' ', '\'', '-', '_', '"', '.', '!', '?', ',', ';', ':', '/', '\\',
                    '=', '+', '(', ')', '{', '}', '&', '|', '#', '§', '*', '[', ']');

                for (int i = 0; i < b.Length; ++i)
                {
                    if (i < b.Length - 1)
                    {
                        ret += b[i].ToUpperFirstCaractere() + str.Substring(ret.Length + b[i].Length, 1);
                    }
                    else
                    {
                        ret += b[i].ToUpperFirstCaractere();
                    }
                }
                return ret;
            }
        }
    }
}
