﻿using DAL_Client.Entities;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			UserService service = new UserService();

            //---GET(OK)
            User user = new User();
            user = service.Get(1);
            Console.WriteLine(user.Name);


            // --- Insert									(OK)	
            //User user1 = new User();
            //user1.Name = "Toto";
            //user1.LastName = "Schilacci";
            //user1.Email = "toto@gmail.com";
            //user1.Login = "toto";
            //user1.Pwd = "test1234=";
            //user1.RoleId = 1;
            //user1.Active = true;
            //user1.Id = service.Insert(user1);

            // --- Delete									(OK)
            //service.Delete(2);

            // --- Update									(OK)
            user = service.Get(1);
            // user.name = "alberto";
            bool test = service.Update(user);
            //Console.WriteLine(test);

            //SpeciesService spService = new SpeciesService();
            //List<Species> species = spService.GetAll().ToList();
            //for (int i = 0; i < species.Count(); i++)
            //{
            //    Console.WriteLine(species[i].LatinName.ToString());                
            //}
            Console.ReadLine();

        }
	}
}
