﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Inaturalist.ModeleApiSearch;
using Newtonsoft.Json;

namespace Inaturalist.ServicesApi
{
    public class InaturalistApiService
    {
        private static string SearchUrl = "http://api.inaturalist.org/v1/search?q=";
        public static async Task<RetourSearchApi> GetAnimalAsync(string path)
        {

            HttpClient client = new HttpClient();
            RetourSearchApi product = null;
            HttpResponseMessage response = await client.GetAsync(SearchUrl+path);
            if (response.IsSuccessStatusCode)
            {
                product =JsonConvert.DeserializeObject<RetourSearchApi>(await response.Content.ReadAsStringAsync());
            }
            return product;
        }
        






    }
}


