﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inaturalist.ModeleApiTaxa;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace Inaturalist.ServicesApi
{
    public class InaturalistApiTaxo
    {

        private static string TaxaUrl = "http://api.inaturalist.org/v1/taxa/";
        public static async Task<RetourTaxaApi> GetDetailAsync(int id)
        {

            HttpClient client = new HttpClient();
            RetourTaxaApi product = null;
            HttpResponseMessage response = await client.GetAsync(TaxaUrl + id);
            if (response.IsSuccessStatusCode)
            {
                product = JsonConvert.DeserializeObject<RetourTaxaApi>(await response.Content.ReadAsStringAsync());
            }
            return product;
        }
    }
}
