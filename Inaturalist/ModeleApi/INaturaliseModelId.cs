﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inaturalist.ModeleApiTaxa
{
        public class FlagCounts
        {
            public int unresolved { get; set; }
            public int resolved { get; set; }
        }

        public class OriginalDimensions
        {
            public int width { get; set; }
            public int height { get; set; }
        }

        public class Photo
        {
            public List<object> flags { get; set; }
            public string type { get; set; }
            public string url { get; set; }
            public string square_url { get; set; }
            public string native_page_url { get; set; }
            public string native_photo_id { get; set; }
            public string small_url { get; set; }
            public string original_url { get; set; }
            public string attribution { get; set; }
            public string medium_url { get; set; }
            public int id { get; set; }
            public string license_code { get; set; }
            public OriginalDimensions original_dimensions { get; set; }
            public string large_url { get; set; }
        }

        public class FlagCounts2
        {
            public int unresolved { get; set; }
            public int resolved { get; set; }
        }

        public class OriginalDimensions2
        {
            public int width { get; set; }
            public int height { get; set; }
        }

        public class DefaultPhoto
        {
            public string square_url { get; set; }
            public string attribution { get; set; }
            public List<object> flags { get; set; }
            public string medium_url { get; set; }
            public int id { get; set; }
            public string license_code { get; set; }
            public OriginalDimensions2 original_dimensions { get; set; }
            public string url { get; set; }
        }

        public class Taxon
        {
            public int taxon_schemes_count { get; set; }
            public string ancestry { get; set; }
            public string min_species_ancestry { get; set; }
            public string wikipedia_url { get; set; }
            public object current_synonymous_taxon_ids { get; set; }
            public int iconic_taxon_id { get; set; }
            public DateTime created_at { get; set; }
            public int taxon_changes_count { get; set; }
            public object complete_species_count { get; set; }
            public string rank { get; set; }
            public bool extinct { get; set; }
            public int id { get; set; }
            public int universal_search_rank { get; set; }
            public List<string> ancestor_ids { get; set; }
            public int observations_count { get; set; }
            public bool is_active { get; set; }
            public FlagCounts2 flag_counts { get; set; }
            public int min_species_taxon_id { get; set; }
            public string rank_level { get; set; }
            public object atlas_id { get; set; }
            public int? parent_id { get; set; }
            public string name { get; set; }
            public DefaultPhoto default_photo { get; set; }
            public string iconic_taxon_name { get; set; }
            public string preferred_common_name { get; set; }
        }

        public class TaxonPhoto
        {
            public int taxon_id { get; set; }
            public Photo photo { get; set; }
            public Taxon taxon { get; set; }
        }

        public class OriginalDimensions3
        {
            public int width { get; set; }
            public int height { get; set; }
        }

        public class DefaultPhoto2
        {
            public string square_url { get; set; }
            public string attribution { get; set; }
            public List<object> flags { get; set; }
            public string medium_url { get; set; }
            public int id { get; set; }
            public string license_code { get; set; }
            public OriginalDimensions3 original_dimensions { get; set; }
            public string url { get; set; }
        }

        public class FlagCounts3
        {
            public int unresolved { get; set; }
            public int resolved { get; set; }
        }

        public class OriginalDimensions4
        {
            public int width { get; set; }
            public int height { get; set; }
        }

        public class DefaultPhoto3
        {
            public string square_url { get; set; }
            public string attribution { get; set; }
            public List<object> flags { get; set; }
            public string medium_url { get; set; }
            public int id { get; set; }
            public string license_code { get; set; }
            public OriginalDimensions4 original_dimensions { get; set; }
            public string url { get; set; }
        }

        public class Ancestor
        {
            public int observations_count { get; set; }
            public int taxon_schemes_count { get; set; }
            public string ancestry { get; set; }
            public bool is_active { get; set; }
            public FlagCounts3 flag_counts { get; set; }
            public string wikipedia_url { get; set; }
            public object current_synonymous_taxon_ids { get; set; }
            public int iconic_taxon_id { get; set; }
            public string rank_level { get; set; }
            public int taxon_changes_count { get; set; }
            public object atlas_id { get; set; }
            public object complete_species_count { get; set; }
            public int parent_id { get; set; }
            public string complete_rank { get; set; }
            public string name { get; set; }
            public string rank { get; set; }
            public bool extinct { get; set; }
            public int id { get; set; }
            public DefaultPhoto3 default_photo { get; set; }
            public List<string> ancestor_ids { get; set; }
            public string iconic_taxon_name { get; set; }
            public string preferred_common_name { get; set; }
        }

        public class Result
        {
            public int observations_count { get; set; }
            public int taxon_schemes_count { get; set; }
            public string ancestry { get; set; }
            public bool is_active { get; set; }
            public FlagCounts flag_counts { get; set; }
            public string wikipedia_url { get; set; }
            public object current_synonymous_taxon_ids { get; set; }
            public int iconic_taxon_id { get; set; }
            public List<TaxonPhoto> taxon_photos { get; set; }
            public string rank_level { get; set; }
            public int taxon_changes_count { get; set; }
            public object atlas_id { get; set; }
            public object complete_species_count { get; set; }
            public int parent_id { get; set; }
            public string name { get; set; }
            public string rank { get; set; }
            public bool extinct { get; set; }
            public int id { get; set; }
            public DefaultPhoto2 default_photo { get; set; }
            public List<int> ancestor_ids { get; set; }
            public string iconic_taxon_name { get; set; }
            public string preferred_common_name { get; set; }
            public List<Ancestor> ancestors { get; set; }
            public List<object> conservation_statuses { get; set; }
            public int listed_taxa_count { get; set; }
            public List<object> listed_taxa { get; set; }
            public string wikipedia_summary { get; set; }
        }

        public class RetourTaxaApi
        {
            public int total_results { get; set; }
            public int page { get; set; }
            public int per_page { get; set; }
            public List<Result> results { get; set; }
        }
}
