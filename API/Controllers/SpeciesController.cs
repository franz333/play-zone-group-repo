﻿using API.Mapper;
using API.Models;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
	[RoutePrefix("api/species")]
	public class SpeciesController : ApiController
    {
		[HttpGet]
		[Route("{id}")]
		public IHttpActionResult Get(int id)
        {
            SpeciesService service = new SpeciesService();

            try
            {
                SpeciesDTO species = service.GetDTO(id).ToAPI();
                return Ok(species);
            }
            catch (Exception)
            {
                return BadRequest("Nous n'avons pas réussi à accéder à l'élément. Veuillez vérifier l'identifiant");
            }
        }

		[HttpGet]
		public IHttpActionResult Get()
        {
            SpeciesService service = new SpeciesService();

            try
            {
                IEnumerable<SpeciesDTO> species = service.GetAllDTO().Select(sp => sp.ToAPI());
                return Ok(species);
            }
            catch (Exception)
            {

                return BadRequest("");
            }
        }

		[HttpGet]
		[Route("byKingdom/{kingdomId}")]
		public IHttpActionResult GetAllByKingdom(int kingdomId)
        {
            SpeciesService service = new SpeciesService();

            try
            {
                IEnumerable<SpeciesDTO> species = service.GetAllSpeciesByKingdomId(kingdomId).Select(sp => sp.ToAPI());
                return Ok(species);
            }
            catch (Exception)
            {
                return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête..."); 
            }        
        }

		[HttpGet]
		[Route("byBranch/{branchId}")]
		public IHttpActionResult GetAllByBranchId(int branchId)
		{
			SpeciesService service = new SpeciesService();

			try
			{
				IEnumerable<SpeciesDTO> species = service.GetAllSpeciesByBranchId(branchId).Select(sp => sp.ToAPI());
				return Ok(species);
			}
			catch (Exception)
			{

				return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête...");
			}
		}

		[HttpGet]
		[Route("bySubBranch/{subBranchId}")]
		public IHttpActionResult GetAllBySubBranch(int subBranchId)
		{
			SpeciesService service = new SpeciesService();

			try
			{
				IEnumerable<SpeciesDTO> species = service.GetAllSpeciesBySubBranchId(subBranchId).Select(sp => sp.ToAPI());
				return Ok(species);
			}
			catch (Exception)
			{
				return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête...");
			}
		}

		[HttpGet]
		[Route("byClass/{classId}")]
		public IHttpActionResult GetAllByClass(int classId)
        {
            SpeciesService service = new SpeciesService();

            try
            {
                IEnumerable<SpeciesDTO> species = service.GetAllSpeciesByClassId(classId).Select(sp => sp.ToAPI());
                return Ok(species);
            }
            catch (Exception)
            {
                return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête...");
            }
        }

		[HttpGet]
		[Route("byOrder/{orderId}")]
		public IHttpActionResult GetAllByOrder(int orderId)
		{
			SpeciesService service = new SpeciesService();

			try
			{
				IEnumerable<SpeciesDTO> species = service.GetAllSpeciesByOrderId(orderId).Select(sp => sp.ToAPI());
				return Ok(species);
			}
			catch (Exception)
			{
				return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête...");
			}
		}		

		[HttpGet]
		[Route("byFamily/{familyId}")]
		public IHttpActionResult GetAllByFamily(int familyId)
		{
			SpeciesService service = new SpeciesService();

			try
			{
				IEnumerable<SpeciesDTO> species = service.GetAllSpeciesByFamilyId(familyId).Select(sp => sp.ToAPI());
				return Ok(species);
			}
			catch (Exception)
			{
				return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête...");
			}
		}

		[HttpGet]
		[Route("byGender/{genderId}")]
		public IHttpActionResult GetAllByGender(int genderId)
		{
			SpeciesService service = new SpeciesService();

			try
			{
				IEnumerable<SpeciesDTO> species = service.GetAllSpeciesByGenderId(genderId).Select(sp => sp.ToAPI());
				return Ok(species);
			}
			catch (Exception)
			{
				return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête...");
			}
		}

	}



}
