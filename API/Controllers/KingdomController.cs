﻿using DAL_Client.Entities;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class KingdomController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            KingdomService service = new KingdomService();

            try
            {
                Kingdom kingdom = service.Get(id);
                return Ok(kingdom);
            }
            catch (Exception)
            {
                return BadRequest("Nous n'avons pas réussi à accéder à l'élément. Veuillez vérifier l'identifiant...");
            }
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            KingdomService service = new KingdomService();

            try
            {
                IEnumerable<Kingdom> kindoms = service.GetAll();
                return Ok(kindoms);
            }
            catch (Exception)
            {
                return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier votre requête...");
            }
        }
    }
}
