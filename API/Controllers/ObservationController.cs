﻿using DAL_Client.Entities;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class ObservationController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Insert(Observation obs)
        {
            ObservationService service = new ObservationService();

            try
            {
                obs.Id = service.Insert(obs);
                return Ok("Votre observation a été correctement enregistrée, merci !");
            }
            catch (Exception)
            {
                return Ok(new
                {
                    error = "Une erreur s'est produite lors de l'insertion."
                }); 
            }           
        }
    }
}
