﻿using DAL_Client.Entities;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class BranchController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            BranchService service = new BranchService();

            try
            {
                Branch branch = service.Get(id);
                return Ok(branch);
            }
            catch (Exception)
            {
                return BadRequest("Nous n'avons pas réussi à accéder à l'élément. Veuillez vérifier l'identifiant...");
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllByKingdomId(int kingdomId)
        {
            BranchService service = new BranchService();

            try
            {
                IEnumerable<Branch> branches = service.GetAllBranchByKingdomId(kingdomId);
                return Ok(branches);
            }
            catch (Exception)
            {
                return BadRequest("Nous n'avons pas réussi à accéder aux éléments. Veuillez vérifier l'identifiant..."); ;
            }
        }
    }
}
