﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CE = DAL_Client.Entities;

namespace API.Mapper
{
    public static class Mapper
    {
        public static SpeciesDTO ToAPI(this CE.SpeciesDTO species)
        {
            return (species == null) ? null : new SpeciesDTO()
            {
                Id = species.Id,
                LatinName = species.LatinName,
                Name = species.Name,
                Description = species.Description,
                PeriodBegin = species.PeriodBegin,
                PeriodEnd = species.PeriodEnd,
                GenderId = species.GenderId,
                GenderLatinName = species.GenderLatinName,
                GenderName = species.GenderName,
                OrderId = species.OrderId,
                OrderLatinName = species.OrderLatinName,
                OrderName = species.OrderName,
                FamilyId = species.FamilyId,
                FamilyLatinName = species.FamilyLatinName,
                FamilyName = species.FamilyName,
                ClassId = species.ClassId,
                ClassLatinName = species.ClassLatinName,
                ClassName = species.ClassName,
                SubBranchId = species.SubBranchId,
                SubBranchLatinName = species.SubBranchLatinName,
                SubBranchName = species.SubBranchName,
                BranchId = species.BranchId,
                BranchLatinName = species.BranchLatinName,
                BranchName = species.BranchName,
                KingdomId = species.KingdomId,
                KingdomLatinName = species.KingdomLatinName,
                KingdomName = species.KingdomName,
            };
        }      
    }
}