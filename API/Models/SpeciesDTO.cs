﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class SpeciesDTO
    {
        [Key]
        public int Id { get; set; }

        public string LatinName { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Period
        {
            get
            {
                return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(PeriodBegin) + " > " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(PeriodEnd);
            }
        }

        public int PeriodBegin { get; set; }

        public int PeriodEnd { get; set; }

        public int SubBranchId { get; set; }

        public string SubBranchLatinName { get; set; }

        public string SubBranchName { get; set; }

        public int GenderId { get; set; }

        public string GenderLatinName { get; set; }

        public string GenderName { get; set; }

        public int FamilyId { get; set; }

        public string FamilyLatinName { get; set; }

        public string FamilyName { get; set; }

        public int OrderId { get; set; }

        public string OrderLatinName { get; set; }

        public string OrderName { get; set; }

        public int ClassId { get; set; }

        public string ClassLatinName { get; set; }

        public string ClassName { get; set; }

        public int BranchId { get; set; }

        public string BranchLatinName { get; set; }

        public string BranchName { get; set; }

        public int KingdomId { get; set; }

        public string KingdomLatinName { get; set; }

        public string KingdomName { get; set; }

        //private IEnumerable<Img> _imgs;

        //public IEnumerable<Img> Imgs
        //{
        //    get
        //    {
        //        if (_imgs == null)
        //        {
        //            ImgService service = new ImgService();
        //            _imgs = service.GetAllImgBySpeciesId(Id);
        //        }
        //        return _imgs;
        //    }
        //}
    }
}