﻿using Admin_Interface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin_Interface.Tools
{
    public static class UserSession
    {
        public static AuthUser AuthUser
        {
            get => (AuthUser)HttpContext.Current.Session["UserRights"];
            set => HttpContext.Current.Session["UserRights"] = value;
        }
    }
}