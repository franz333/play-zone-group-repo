﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin_Interface.Tools
{
    public static class CustomExtensionsMethods
    {
        public static string ToOuiNon(this bool b)
        {
            return b ? "Oui" : "Non";
        }
    }
}