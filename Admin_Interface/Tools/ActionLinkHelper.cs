﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Routing;

namespace Admin_Interface.Tools
{
    public static class ActionLinkHelper
    {
        public static IHtmlString ImageAjaxActionLink(this AjaxHelper helper, string imageUrl, string actionName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes, string title = null)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", imageUrl);
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            var link = helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions).ToHtmlString();

            if (title != null)
            {
                var builderText = new TagBuilder("span");
                builderText.InnerHtml = title;
                return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing) + builderText.ToString(TagRenderMode.Normal)));
            }
            else
            {
                return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing)));
            }
        }

        public static IHtmlString ImageActionLink(this HtmlHelper htmlHelper, string imageUrl, string actionName, string controller, object routeValues, object htmlAttributes, string alt = null, string imgTitle = null, string title = null)
        {
            // Création du tag <img> avec différents attributs
            var builderImg = new TagBuilder("img");
            builderImg.MergeAttribute("src", imageUrl);

            if (alt != null)
            {
                builderImg.MergeAttribute("alt", alt);
            }

            if (imgTitle != null)
            {
                builderImg.MergeAttribute("title", imgTitle);
            }

            builderImg.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            // Création du tag <a> avec le href
            var builderAnchor = new TagBuilder("a");
            builderAnchor.InnerHtml = builderImg.ToString(TagRenderMode.SelfClosing);

            // Création du tag <span> avec le texte
            if (title != null)
            {
                var builderText = new TagBuilder("span");
                builderText.InnerHtml = title;
                builderAnchor.InnerHtml = builderImg.ToString(TagRenderMode.SelfClosing) + builderText.ToString(TagRenderMode.Normal);
            }

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            builderAnchor.Attributes["href"] = urlHelper.Action(actionName, controller, routeValues);
            builderAnchor.MergeAttributes(new RouteValueDictionary());

            return MvcHtmlString.Create(builderAnchor.ToString());
        }

    }
}