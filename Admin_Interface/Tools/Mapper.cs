﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASP = Admin_Interface.Models;
using ASPVM_SP = Admin_Interface.Models.ViewModels.SpeciesViews;
using DC = DAL_Client.Entities;

namespace Admin_Interface.Tools
{
	public static class Mapper
	{
		public static ASP.User ToASP(this DC.User user)
		{
			// si le user de la Dal Client est null, alors retournes null sinon un nouveau ASP user
			return (user == null) ? null : new ASP.User()
			{
				Active = user.Active,
				Email = user.Email,
				Id = user.Id,
				LastName = user.LastName,
				Login = user.Login,
				Name = user.Name,
				Pwd = "*************",
				RoleId = user.RoleId,
				RoleType = user.RoleType,
			};
		}

		public static DC.User ToDC(this ASP.User user)
		{
			return (user == null) ? null : new DC.User()
			{
				Active = user.Active,
				Email = user.Email,
				Id = user.Id,
				LastName = user.LastName,
				Login = user.Login,
				Name = user.Name,
				Pwd = user.Pwd.ApplyHash(),
				RoleId = user.RoleId,
				RoleType = user.RoleType,
			};
		}

		public static ASP.Role ToASP(this DC.Role role)
		{
			return (role == null) ? null : new ASP.Role()
			{
				Id = role.Id,
				Type = role.Type,
			};
		}

		public static ASP.Kingdom ToASP(this DC.Kingdom kingdom)
		{
			return (kingdom == null) ? null : new ASP.Kingdom()
			{
				Id = kingdom.Id,
				LatinName = kingdom.LatinName,
				Name = kingdom.Name,
			};
		}

		public static ASPVM_SP.SpeciesAdmin ToASP(this DC.SpeciesDTO species)
		{
			return (species == null) ? null : new ASPVM_SP.SpeciesAdmin()
			{
				Id = species.Id,
				Species = species.Species,
				Description = species.Description,
				StatusId = species.StatusId,
				StatusType = species.StatusType,
				Period = species.Period,
				Publisher = species.Publisher,
				PublisherId = species.PublisherId,
				Gender = species.Gender,
				Order = species.Order,
				Family = species.Family,
				Class = species.Class,
				SubBranch = species.SubBranch,
				Branch = species.Branch,
				Kingdom = species.Kingdom,
			};
		}

		public static DC.Species ToDC(this ASP.Species species)
		{
			return (species == null) ? null : new DC.Species()
			{
				Description = species.Description,
				LatinName = species.LatinName,
				Name = species.Name,
				PeriodBegin = species.PeriodBegin,
				PeriodEnd = species.PeriodEnd,
				GenderId = species.GenderId,
				PublisherId = species.PublisherId,
				StatusId = species.StatusId,
			};
		}

		public static ASP.Species ToASP(this DC.Species species)
		{
			return (species == null) ? null : new ASP.Species()
			{
				Id = species.Id,
				Description = species.Description,
				LatinName = species.LatinName,
				Name = species.Name,
				PeriodBegin = species.PeriodBegin,
				PeriodEnd = species.PeriodEnd,
				GenderId = species.GenderId,
				PublisherId = species.PublisherId,
				StatusId = species.StatusId,
			};
		}

		//public static ASPVM_SP.SpeciesForm ToASP(this DC.SpeciesDTO species)
		//{ 
		//	return (species == null) ? null : new ASPVM_SP.SpeciesForm()
		//	{ 
		//		PeriodBegin = species.PeriodBegin,
		//		PeriodEnd = species.PeriodEnd,
				

		//	}
		//}

		public static DC.Kingdom ToDC(this ASP.Kingdom kingdom)
		{
			return (kingdom == null) ? null : new DC.Kingdom()
			{
				Id = kingdom.Id,
				LatinName = kingdom.LatinName,
				Name = kingdom.Name,
			};
		}

		public static ASP.Branch ToASP(this DC.Branch branch)
		{
			return (branch == null) ? null : new ASP.Branch()
			{
				Id = branch.Id,
				KingdomId = branch.KingdomId,
				LatinName = branch.LatinName,
				Name = branch.Name,
			};
		}

		public static DC.Branch ToDC(this ASP.Branch branch)
		{
			return (branch == null) ? null : new DC.Branch()
			{
				Id = branch.Id,
				KingdomId = branch.KingdomId,
				LatinName = branch.LatinName,
				Name = branch.Name,
			};
		}

		public static ASP.SubBranch ToASP(this DC.SubBranch subBranch)
		{
			return (subBranch == null) ? null : new ASP.SubBranch()
			{
				Id = subBranch.Id,
				LatinName = subBranch.LatinName,
				Name = subBranch.Name,
				BranchId = subBranch.BranchId,
			};
		}

		public static ASP.Class ToASP(this DC.Class spClass)
		{
			return (spClass == null) ? null : new ASP.Class()
			{
				Id = spClass.Id,
				BranchId = spClass.BranchId,
				SubBranchId = spClass.SubBranchId,
				LatinName = spClass.LatinName,
				Name = spClass.Name,
			};
		}

		public static DC.Class ToDC(this ASP.Class spClass)
		{
			return (spClass == null) ? null : new DC.Class()
			{
				Id = spClass.Id,
				BranchId = spClass.BranchId,
				SubBranchId = spClass.SubBranchId,
				LatinName = spClass.LatinName,
				Name = spClass.Name,
			};
		}

		public static ASP.Order ToASP(this DC.Order order)
		{
			return (order == null) ? null : new ASP.Order()
			{
				Id = order.Id,
				LatinName = order.LatinName,
				Name = order.Name,
				ClassId = order.ClassId,
			};
		}

		public static DC.Order ToDC(this ASP.Order order)
		{
			return (order == null) ? null : new DC.Order()
			{
				Id = order.Id,
				LatinName = order.LatinName,
				Name = order.Name,
				ClassId = order.ClassId,
			};
		}

		public static ASP.Family ToASP(this DC.Family family)
		{
			return (family == null) ? null : new ASP.Family()
			{
				Id = family.Id,
				LatinName = family.LatinName,
				Name = family.Name,
				OrderId = family.OrderId,
			};
		}

		public static DC.Family ToDC(this ASP.Family family)
		{
			return (family == null) ? null : new DC.Family()
			{
				Id = family.Id,
				LatinName = family.LatinName,
				Name = family.Name,
				OrderId = family.OrderId,
			};
		}

		public static ASP.Gender ToASP(this DC.Gender gender)
		{
			return (gender == null) ? null : new ASP.Gender()
			{
				Id = gender.Id,
				FamilyId = gender.FamilyId,
				LatinName = gender.LatinName,
				Name = gender.Name,
			};
		}

		public static DC.Gender ToDC(this ASP.Gender gender)
		{
			return (gender == null) ? null : new DC.Gender()
			{
				Id = gender.Id,
				FamilyId = gender.FamilyId,
				LatinName = gender.LatinName,
				Name = gender.Name,
			};
		}

		public static DC.Observation ToDC(this ASP.ObservationAdmin observationAdmin)
		{
			return (observationAdmin == null) ? null : new DC.Observation()
			{
				Content = observationAdmin.Content,
				DateOfObs = observationAdmin.DateOfObs,
				DateOfPost = observationAdmin.DateOfPost,
				Id = observationAdmin.Id,
				ImgId = observationAdmin.ImgId,
				Latitude = observationAdmin.Latitude,
				Longitude = observationAdmin.Longitude,
				StatusId = observationAdmin.StatusId,
				ValidatorId = observationAdmin.ValidatorId,
				SpeciesId = observationAdmin.SpeciesId,
			};
		}

		public static ASP.ObservationAdmin ToASP(this DC.Observation observationAdmin)
		{
			return (observationAdmin == null) ? null : new ASP.ObservationAdmin()
			{
				Content = observationAdmin.Content,
				DateOfObs = observationAdmin.DateOfObs,
				DateOfPost = observationAdmin.DateOfPost,
				Id = observationAdmin.Id,
				ImgId = observationAdmin.ImgId,
				Latitude = observationAdmin.Latitude,
				Longitude = observationAdmin.Longitude,
				StatusId = observationAdmin.StatusId,
				ValidatorId = observationAdmin.ValidatorId,
				SpeciesId = observationAdmin.SpeciesId,
			};
		}

		public static ASP.Img ToASP(this DC.Img img)
        {
            return (img == null) ? null : new ASP.Img()
            {
                CopyRight = img.CopyRight,
                Data = img.Data,
                Date = img.Date,
                Id = img.Id,
                ObservationId = img.ObservationId,
                Origin = img.Origin,
                SpeciesId = img.SpeciesId,
            };        
        }

        public static DC.Img ToDC(this ASP.Img img)
        {
            return (img == null) ? null : new DC.Img()
            {
                CopyRight = img.CopyRight,
                Data = img.Data,
                Date = img.Date,
                Id = img.Id,
                ObservationId = img.ObservationId,
                Origin = img.Origin,
                SpeciesId = img.SpeciesId,
                Format = img.Format,
            };
        }
	}
 }