﻿using Admin_Interface.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_Interface.Models
{
    public class User
    {
        [ReadOnly(true)]
        public int Id { get; set; }

        [Display(Name = "Nom")]
        public string Name { get; set; }

        [Display(Name = "Prénom")]
        public string LastName { get; set; }

        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [HiddenInput]
        public string Login { get; set; }

        [HiddenInput]
        [Display(Name = "Mot de passe")]
        public string Pwd { get; set; }

        [HiddenInput]
        public bool Active { get; set; }

        [Display(Name = "Actif")]
        public string IsActive
        {
            get { return Active.ToOuiNon(); }
        }

        [HiddenInput]
        public int RoleId { get; set; }

        [Display(Name = "Role")]
        public string RoleType { get; set; }

    }
}