﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Admin_Interface.Models.ViewModels.SpeciesViews
{
	public class SpeciesAdmin 
	{
		// Species Details

		public int Id { get; set; }

		[Display(Name = "Espèce")]
		public string Species { get; set; }   

		public string Description { get; set; }

		[Display(Name = "Période")]
		public string Period { get; set; }

		public int PublisherId { get; set; }

		[Display(Name = "Editeur")]
		public string Publisher { get; set; }

		public int StatusId { get; set; }

		[Display(Name = "Status")]
		public string StatusType { get; set; }

		// Taxonomy Section

		[Display(Name = "Royaume")]
		public string Kingdom { get; set; }

		[Display(Name = "Branche")]
		public string Branch { get; set; }

		[Display(Name = "Sous-branche")]
		public string SubBranch { get; set; }

		[Display(Name = "Classe")]
		public string Class { get; set; }

		[Display(Name = "Genre")]
		public string Gender { get; set; }

		[Display(Name = "Famille")]
		public string Family { get; set; }

		[Display(Name = "Ordre")]
		public string Order { get; set; }				

		public IEnumerable<Img> Imgs;
	}
}
