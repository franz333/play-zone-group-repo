﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Admin_Interface.Tools;
using DAL_Client.Services;

namespace Admin_Interface.Models.ViewModels.SpeciesViews
{
	public class SpeciesForm : Species
	{
		private int _kingdomId;

		[Display(Name = "Royaume")]
		public int KingdomId
		{
			get { return _kingdomId; }
			set { _kingdomId = value; }
		}

		private int _branchId;

		[Display(Name = "Branche")]
		public int BranchId
		{
			get { return _branchId; }
			set { _branchId = value; }
		}

		private int _subBranchId;

		[Display(Name = "Sous-branche")]
		public int SubBranchId
		{
			get { return _subBranchId; }
			set { _subBranchId = value; }
		}
				
		private int _classId;

		[Display(Name = "Classe")]
		public int ClassId
		{
			get { return _classId; }
			set { _classId = value; }
		}

		private int _orderId;

		[Display(Name = "Ordre")]
		public int OrderId
		{
			get { return _orderId; }
			set { _orderId = value; }
		}

		private int _familyId;

		[Display(Name = "Famille")]
		public int FamilyId
		{
			get { return _familyId; }
			set { _familyId = value; }
		}
		
		public enum ChosePeriod
		{
			Janvier = 1,
			Février = 2,
			Mars = 3,
			Avril = 4,
			Mai = 5,
			Juin = 6,
			Juillet = 7,
			Août = 8,
			Septembre = 9,
			Octobre = 10,
			Novembre = 11,
			Décembre = 12
		}

		public ChosePeriod Begin { get; set; }

		public ChosePeriod End { get; set; }

		public Species SaveSpecies()
		{
			SpeciesService service = new SpeciesService();
			Species species = new Species();
			species.Description = Description;
			species.LatinName = LatinName;
			species.Name = Name;
			species.Description = Description;
			species.PeriodBegin = (int)Begin;
			species.PeriodEnd = (int)End;
			species.GenderId = GenderId;
			species.PublisherId = PublisherId;
			species.StatusId = StatusId;
			species.Id = service.Insert(species.ToDC());
			return species;
		}

		// CTOR
		public SpeciesForm()
		{
		}
	}
}