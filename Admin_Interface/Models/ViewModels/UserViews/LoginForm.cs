﻿using Admin_Interface.Tools;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Admin_Interface.Models.ViewModels.UserViews
{
    public class LoginForm
    {
        [Required]
        [Display(Name = "Identifiant")]
        public string Login { get; set; }

        [Required]
        [Display(Name = "Mot de passe")]
        public string Pwd { get; set; }

        // Vérification que l'utilisateur existe, si oui on créé une session
        public bool LoginCheck()
        {
            UserService service = new UserService();
			User user = new User();
			
			user.Pwd = this.Pwd;
			user.Login = this.Login;

			user = service.Login(user.ToDC()).ToASP();

			if (user != null)
			{
				UserSession.AuthUser = new AuthUser()
				{
					Id = user.Id,
					RoleId = user.RoleId,
					RoleType = user.RoleType,
					// TODO = changer en Name après avoir inversé les valeurs en DB
					Name = user.LastName,
				};
				return true;
			}
			else
			{
				return false;
			}
		}
    }
}