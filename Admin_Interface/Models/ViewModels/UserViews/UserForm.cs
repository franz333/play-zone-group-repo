﻿using Admin_Interface.Tools;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_Interface.Models.ViewModels.UserViews
{
    public class UserForm
    {
		private User _user;

		private List<Role> _roles;

		[Display(Name = "Rôle")]
		public List<Role> Roles
		{
			get
			{
				RoleService service = new RoleService();
				_roles = service.GetAll().Select(r => r.ToASP()).ToList();
				return _roles;
			}
			private set => _roles = value;
		}

		[Display(Name = "Nom")]
        public string Name 
        {
            get => _user.Name;
            set => _user.Name = value;
        }

        [Display(Name = "Prénom")]
        public string LastName 
        {
            get => _user.LastName;
            set => _user.LastName = value;
        }

        [Display(Name = "Login")]
        public string Login 
        {
            get => _user.Login;
            set => _user.Login = value;
        }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Un mot de passe est requis")]
        [Display(Name = "Mot de passe")]
        public string Pwd 
        {
            get => _user.Pwd;
            set => _user.Pwd = value;
        }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Une adresse e-mail est requise")]
        [Display(Name = "E-Mail")]
        public string Email 
        {
            get => _user.Email;
            set => _user.Email = value;
        }

        [HiddenInput(DisplayValue = false)]
        public string RoleType 
        {
            get => _user.RoleType;
            set => _user.RoleType = value; 
        }

		[HiddenInput(DisplayValue = false)]
		public int RoleId
		{
			get => _user.RoleId;
			set => _user.RoleId = value;
		}

		[HiddenInput(DisplayValue = false)]
		public bool Active
		{
			get => _user.Active;
			set => _user.Active = value;
		}

		public UserForm()
		{
			_user = new User();
		}

		public User SaveUser()
		{   
			UserService service = new UserService();
			_user.Id = service.Insert(_user.ToDC());
			return _user;
		}
    }
}