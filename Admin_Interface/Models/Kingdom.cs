﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Admin_Interface.Models
{
	public class Kingdom
	{
		[Key]
		public int Id { get; set; }

		public string LatinName { get; set; }

		[Display(Name = "Royaume")]
		public string Name { get; set; }
	}
}