﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin_Interface.Models
{
    public class AuthUser
    {
        public int Id { get; set; }

        public int RoleId { get; set; }

        public string RoleType { get; set; }

		public string Name { get; set; }
    }
}