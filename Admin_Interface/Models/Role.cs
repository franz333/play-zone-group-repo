﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin_Interface.Models
{
    public class Role
    {
        public int Id { get; set; }

        public string Type { get; set; }
    }
}