﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Admin_Interface.Models
{
    public class Species 
    {
        [Key]
		public int Id { get; set; }

		[Display(Name = "Nom")]
		public string Name { get; set; }

		[Display(Name = "Nom Latin")]
		public string LatinName { get; set; }

		[Display(Name = "Description")]
		public string Description { get; set; }

		[Display(Name = "Début de la période")]
		public int PeriodBegin { get; set; }

		[Display(Name = "Fin de la période")]
		public int PeriodEnd { get; set; }

		[Display(Name = "Genre")]
		public int GenderId { get; set; }

		public int StatusId { get; set; }

		public int PublisherId { get; set; }

	}
}