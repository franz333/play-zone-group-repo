﻿using Admin_Interface.Models.ViewModels.UserViews;
using Admin_Interface.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_Interface.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        [AnonymousRequired]
        public ActionResult Index()
        {
            LoginForm form = new LoginForm();
            return View("Index", form);
        }

        [HttpPost]
        [AnonymousRequired]
        public ActionResult Index(LoginForm form, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (form.LoginCheck())
                {
                    if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "DashBoard", new { id = UserSession.AuthUser.Id });
                }
            }
            return View(form);
        }

        public ActionResult Logout()
        {
            HttpContext.Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}