﻿using Admin_Interface.Models;
using Admin_Interface.Models.ViewModels.SpeciesViews;
using Admin_Interface.Models.ViewModels.UserViews;
using Admin_Interface.Tools;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Admin_Interface.Controllers
{
    //[AuthRequired]
    public class DashBoardController : Controller
    {
        // GET: DashBoard
        [HttpGet]
        public ActionResult Index()
        {
            // SuperAdmin
            if (UserSession.AuthUser.RoleId == 1)
            {
				// nombre de fiches
				SpeciesService spService = new SpeciesService();
				List<Species> species = spService.GetAll().Select(s => s.ToASP()).Where(s => s.Id != 1).ToList();

				int spTotal = species.Count();
				int spDraft = 0;
				int spToTreat = 0;
				int spPublished = 0;

				for (int i = 0; i < spTotal; i++)
				{
					if (species[i].StatusId == 1)
					{
						spToTreat += 1;
					}
					else if (species[i].StatusId == 2)
					{
						spDraft += 1;
					}
					else if (species[i].StatusId == 4)
					{
						spPublished += 1;
					}
				}					
				ViewBag.SpeciesCount = spTotal;
				ViewBag.SpeciesDraft = spDraft;
				ViewBag.SpeciesToTreat = spToTreat;
				ViewBag.SpeciesPublished = spPublished;

				// nombre d'observation
				ObservationService oService = new ObservationService();
				List<ObservationAdmin> observations = oService.GetAll().Select(o => o.ToASP()).ToList();

				int obTotal = observations.Count();
				int obDraft = 0;
				int obValidated = 0;
				int obToTreat = 0;
				int obPublished = 0;

				for (int i = 0; i < obTotal; i++)
				{
					if (observations[i].StatusId == 1)
					{
						obToTreat += 1;
					}
					else if (observations[i].StatusId == 2)
					{
						obDraft += 1;
					}
					else if (observations[i].StatusId == 3)
					{
						obValidated += 1;
					}
					else if (observations[i].StatusId == 4)
					{
						obPublished += 1;
					}
				}

				ViewBag.ObsCount = obTotal;
				ViewBag.ObsDraft = obDraft;
				ViewBag.ObsValidated = obValidated;
				ViewBag.ObsToTreat = obToTreat;
				ViewBag.ObsPublished = obPublished;

				// nombre d'utilisateur
				UserService uService = new UserService();
				List<User> users = uService.GetAll().Select(u => u.ToASP()).ToList();

				int uTotal = users.Count();
				int uSuperAdmin = 0;
				int uAdmin = 0;
				int uAssistant = 0;

				for (int i = 0; i < uTotal; i++)
				{
					if (users[i].RoleId == 1)
					{
						uSuperAdmin += 1;
					}
					else if (users[i].RoleId == 2)
					{
						uAdmin += 1;
					}
					else 
					{
						uAssistant += 1;
					}
				}

				ViewBag.UCount = uTotal;
				ViewBag.USuperAdmin = uSuperAdmin;
				ViewBag.UAdmin = uAdmin;
				ViewBag.UAssistant = uAssistant;

				return View("SuperAdmin");
            }
            return View("Index");
        }

		//TODO : API Weather
		public async Task<ActionResult> ConsumeExternalAPI()
		{
			string token = "APPID=3041cf6b699f3cca03108f762e7c8ffa";
			string baseurl = "http://api.openweathermap.org/data/2.5/weather?q=Ciney&";
			string url = baseurl + token;

			using (HttpClient client = new HttpClient())
			{
				client.BaseAddress = new Uri(url);
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new
				System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

				System.Net.Http.HttpResponseMessage response = await client.GetAsync(url);
				if (response.IsSuccessStatusCode)
				{
					var data = await response.Content.ReadAsStringAsync();
					var table =
					Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

					System.Web.UI.WebControls.GridView gView = new
					System.Web.UI.WebControls.GridView();
					gView.DataSource = table;
					gView.DataBind();
					using (System.IO.StringWriter sw = new System.IO.StringWriter())
					{
						using (System.Web.UI.HtmlTextWriter htw = new
						System.Web.UI.HtmlTextWriter(sw))
						{
							gView.RenderControl(htw);
							ViewBag.ReturnedData = sw.ToString();
						}
					}
				}
			}
			return View();
		}

		[HttpGet]
        public ActionResult Users()
        {
			if (UserSession.AuthUser == null || UserSession.AuthUser.RoleId != 1)
			{
				return RedirectToAction("Index", "Login");
			}
			else
			{
				UserService service = new UserService();
				IEnumerable<User> users = service.GetAll().Select(u => u.ToASP());
				return View("Users", users);
			}            
		}

        [HttpGet]
        public ActionResult CreateUser()
        {
            // SuperAdmin
            if (UserSession.AuthUser.RoleId == 1)
            {
                UserForm form = new UserForm();
                return PartialView("_CreateUser", form);
            }
            return RedirectToAction("DashBoard", "Index");
        }

        [HttpPost]
        public ActionResult CreateUser(UserForm form)
        {
			if (!ModelState.IsValid)
			{
				return PartialView("_CreateUser", form);
			}

			User user = form.SaveUser();
			//return PartialView("_UserRowTr", user);
			return RedirectToAction("Users", "DashBoard");
		}

        [HttpGet]
        public ActionResult Deactivate(int id)
        {
			// Désactivation du User (Active = false)
            UserService service = new UserService();
            bool result = service.Deactivate(id);

            if (result)
            {
                // Récupération du User pour update du champ
                User user = new User();
                user = service.Get(id).ToASP();
				return PartialView("_UserRow", user);
			}
			// TODO : Si pas bon, afficher modal
			throw new UnauthorizedAccessException();
		}

        [HttpGet]
        public ActionResult Activate(int id)
        {
            // Activation du User (Active = true)
            UserService service = new UserService();
            bool result = service.Activate(id);

            if (result)
            {
                // Récupération du User pour update du champ
                User user = new User();
                user = service.Get(id).ToASP();
                return PartialView("_UserRow", user);
				// TODO : Si pas bon, afficher modal
            }
			throw new UnauthorizedAccessException();
		}

        [HttpGet]
        public ActionResult Species()
        {
            SpeciesService service = new SpeciesService();
			IEnumerable<SpeciesAdmin> species = service.GetAllDTO().Select(p => p.ToASP());
            return View("Species", species);
        }

		#region Create Species
		[HttpGet]
		public ActionResult CreateSpecies()
		{
			SpeciesForm form = new SpeciesForm();
			// TODO : choper l'Id du publisher par Usersession
			// En attendant, on prend le SA
			form.PublisherId = 1;
			return PartialView("_CreateSpecies", form);
		}

		[HttpPost]
		public ActionResult CreateSpecies(SpeciesForm form)
		{
			SpeciesService service = new SpeciesService();
			Species species = new Species();
			// TODO : Définir le statut en fonction de l'actionName
			// Pour l'instant CreateSpecies = brouillon
			form.StatusId = 2;
			species = form.SaveSpecies();

			SpeciesAdmin sp = new SpeciesAdmin();
			sp = service.GetDTO(species.Id).ToASP();
			return RedirectToAction("SpeciesDetails", "DashBoard", new { id = species.Id });
		}

		//public ActionResult ModifySpecies(int id)
		//{
		//	SpeciesService service = new SpeciesService();
		//	SpeciesForm form = new SpeciesForm();
		//	form = service.Get(id).ToASP();
		//}
			   
		//[HttpGet]
		//public ActionResult Species(int id)
		//{
		//	SpeciesService service = new SpeciesService();
		//	SpeciesAdmin sp = new SpeciesAdmin();
		//	sp = service.GetDTO(id).ToASP();				
		//	return View(sp);
		//}

		[HttpGet]
		public ActionResult SpeciesDetails(int id)
		{
			SpeciesService service = new SpeciesService();
			SpeciesAdmin sp = new SpeciesAdmin();
			sp = service.GetDTO(id).ToASP();
			return View("SpeciesDetails", sp);
		}

        [HttpGet]
        public ActionResult SpeciesTaxonomy(int id)
        {
            SpeciesService service = new SpeciesService();
            SpeciesAdmin sp = new SpeciesAdmin();
            sp = service.GetDTO(id).ToASP();
            return PartialView("_SpeciesTaxo", sp);
        }

        [HttpGet]
        public ActionResult SpeciesImages(int id)
        {
            ImgService service = new ImgService();
            IEnumerable<Img> imgs = service.GetAllImgBySpeciesId(id).Select(i => i.ToASP());
            return PartialView("_SpeciesImages", imgs);
        }

        //[HttpGet]
        //public ActionResult SpeciesAddImages(int id)
        //{ 
            
        //}

		public JsonResult GetKingdoms()
		{
			List<Kingdom> kingdoms = new List<Kingdom>();
			KingdomService service = new KingdomService();
			kingdoms = service.GetAll().Select(k => k.ToASP()).OrderBy(k => k.LatinName).ToList();
			return Json(kingdoms, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetBranches(int kingdomId)
		{
			List<Branch> branches = new List<Branch>();
			BranchService service = new BranchService();
			branches = service.GetAllBranchByKingdomId(kingdomId).Select(b => b.ToASP()).OrderBy(k => k.LatinName).ToList();
			branches.OrderBy(b => b.LatinName);
			return Json(branches, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetSubBranches(int branchId)
		{
			List<SubBranch> subBranches = new List<SubBranch>();

			SubBranchService service = new SubBranchService();
			subBranches = service.GetAllSubBranchByBranchId(branchId).Select(b => b.ToASP()).OrderBy(k => k.LatinName).ToList();
			subBranches.OrderBy(s => s.LatinName);
			return Json(subBranches, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetClasses(int branchId)
		{
			List<Class> classes = new List<Class>();
			ClassService service = new ClassService();
			classes = service.GetAllClassByBranchId(branchId).Select(b => b.ToASP()).OrderBy(k => k.LatinName).ToList();
			return Json(classes, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetClassesBySubBranch(int subBranchId)
		{
			List<Class> classes = new List<Class>();
			ClassService service = new ClassService();
			classes = service.GetAllClassBySubBranchId(subBranchId).Select(b => b.ToASP()).OrderBy(k => k.LatinName).ToList();
			return Json(classes, JsonRequestBehavior.AllowGet);
		}		

		public JsonResult GetOrders(int classId)
		{
			List<Order> orders = new List<Order>();
			OrderService service = new OrderService();
			orders = service.GetAllOrderByClassId(classId).Select(b => b.ToASP()).OrderBy(k => k.LatinName).ToList();
			return Json(orders, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetFamilies(int orderId)
		{
			List<Family> families = new List<Family>();
			FamilyService service = new FamilyService();
			families = service.GetAllFamilyByOrderId(orderId).Select(f => f.ToASP()).OrderBy(f => f.LatinName).ToList();
			return Json(families, JsonRequestBehavior.AllowGet);
		}
		public JsonResult GetGenders(int familyId)
		{
			List<Gender> genders = new List<Gender>();
			GenderService service = new GenderService();
			genders = service.GetAllGenderByFamilyId(familyId).Select(f => f.ToASP()).OrderBy(f => f.LatinName).ToList();
			return Json(genders, JsonRequestBehavior.AllowGet);
		}
		#endregion
	}
}