﻿using Admin_Interface.Models;
using Admin_Interface.Models.ViewModels.UserViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_Interface.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        [HttpGet]
        public ActionResult Index()
        {
            UserForm form = new UserForm();
            return View(form);
        }

        [HttpPost]
        public ActionResult Index(UserForm form)
        {
            User user = new User();
            user = form.SaveUser();
            return View(form);
        }
    }
}