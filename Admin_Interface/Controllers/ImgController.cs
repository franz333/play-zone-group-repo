﻿using Admin_Interface.Models;
using Admin_Interface.Tools;
using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_Interface.Controllers
{
    public class ImgController : Controller
    {
        // GET: Img
        public ActionResult Get(int id)
        {
            ImgService imgService = new ImgService();
            Img img = new Img();
            img = imgService.Get(id).ToASP();
            byte[] imgByteArray = img.Data;
            string arg = "data:image/jpeg;base64,/";
            return File(imgByteArray, arg);
        }
    }
}