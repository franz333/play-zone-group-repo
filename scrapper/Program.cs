﻿using DAL_Global.Entities;
using DAL_Global.Interfaces;
using DAL_Global.Services;
using Inaturalist.ModeleApiSearch;
using Inaturalist.ModeleApiTaxa;
using Inaturalist.ServicesApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace NaturalisScrapper
{
    class Program
    {
        static void Main(string[] args)
        {
            //récupère les especes a chercher depuis un fichier texte
            string[] toScrap = System.IO.File.ReadAllLines(@"Documents\testScrapper.txt");

            List<Record> toMap = new List<Record>();
            List<RetourTaxaApi> apiTaxa = new List<RetourTaxaApi>();
            foreach (string s in toScrap)
            {
                RetourSearchApi result = InaturalistApiService.GetAnimalAsync(s).Result;
                Record record = result?.results.Count > 0 ? result.results[0].record : null;
                Console.WriteLine(record?.name);
                if (record != null)
                {
                    toMap.Add(record);
                    RetourTaxaApi test = InaturalistApiTaxo.GetDetailAsync(record.id).Result;
                    Console.WriteLine(test?.results[0].name);
                    if (test?.results[0] != null)
                    {
                        apiTaxa.Add(test);
                    }
                }
                foreach (RetourTaxaApi taxa in apiTaxa)
                {
                    //je declare un i qui sera l'indice donne dans ancester_ids[i]
                    int i = 1;
                    KingdomService serviceK = new KingdomService();

                    // se sera le kingdom 
                    int ancestor_ids = taxa.results[0].ancestor_ids[i];
                    //j'utilise ma métode pour get via l'api le resulta de ancestor_id(qui est un id)
                    //pour obtenir un object RetourTaxaApi dans le quel il y aura le name ect...
                    RetourTaxaApi retourTaxaApi = InaturalistApiTaxo.GetDetailAsync(ancestor_ids).Result;
                    //j'instencie un bool ou j'utilise le service getall avec la condition
                    //where pour chaque king je compare le name dans la db et je verifie que dans le retour de 
                    //mon api que mon name est oui ou non dedan 
                    Kingdom kingdom = serviceK.GetAll().Where(king => king.Name == retourTaxaApi.results[0].name).FirstOrDefault();
                    //je dis que mon bool est different de null 
                    bool dejaDansDb = kingdom != null;
                    //si il n' est pas dans ma db alor je fais 
                    if (!dejaDansDb)
                    {
                        //j'instencie un new kingdom 
                        kingdom = new Kingdom
                        {
                            //je lui ajout (dans la db son name latin et son name 
                            Name = retourTaxaApi.results[0].preferred_common_name,
                            LatinName = retourTaxaApi.results[0].name
                        };
                        kingdom.Id = serviceK.Insert(kingdom);
                    }
                    i++;
                    BranchService serviceB = new BranchService();
                    int ancestor_ids2 = taxa.results[0].ancestor_ids[i];
                    RetourTaxaApi retourTaxaApiB = InaturalistApiTaxo.GetDetailAsync(ancestor_ids2).Result;
                    Branch branch = serviceB.GetAll().Where(king => king.Name == retourTaxaApi.results[0].name).FirstOrDefault();
                    bool dejaDansDb2 = branch != null;
                    if (!dejaDansDb2)
                    {

                        branch = new Branch
                        {
                            KingdomId = kingdom.Id,
                            Name = retourTaxaApi.results[0].preferred_common_name,
                            LatinName = retourTaxaApi.results[0].name
                        };
                        branch.Id = serviceB.Insert(branch);
                    }



                    

                    i++;
                    int ancestor_ids3 = taxa.results[0].ancestor_ids[i];
                    SubBranch subbranch = null;
                    RetourTaxaApi retourTaxaApiS = InaturalistApiTaxo.GetDetailAsync(ancestor_ids3).Result;
                    if (retourTaxaApiS.results[0].rank == "subphylum")
                    {
                        SubBranchService serviceS = new SubBranchService();
                        subbranch = serviceS.GetAll().Where(king => king.Name == retourTaxaApi.results[0].name).FirstOrDefault();
                        bool dejaDansDb3 = subbranch != null;
                        if (!dejaDansDb2)
                        {

                            subbranch = new SubBranch
                            {
                                BranchId = branch.Id,
                                Name = retourTaxaApi.results[0].preferred_common_name,
                                LatinName = retourTaxaApi.results[0].name
                            };
                            subbranch.Id = serviceS.Insert(subbranch);
                           
                        }

                        i++;
                    }
                    ClassService serviceC = new ClassService();
                    int ancestor_ids4 = taxa.results[0].ancestor_ids[i];
                    RetourTaxaApi retourTaxaApiC = InaturalistApiTaxo.GetDetailAsync(ancestor_ids4).Result;
                    Class item = serviceC.GetAll().Where(king => king.Name == retourTaxaApi.results[0].name).FirstOrDefault();
                    bool dejaDansDb4 = item != null;
                    if (!dejaDansDb4)
                    {

                        item = new Class
                        {
                            SubBranchId = subbranch?.Id,
                            BranchId = branch.Id,
                            Name = retourTaxaApi.results[0].preferred_common_name,
                            LatinName = retourTaxaApi.results[0].name
                        };
                        item.Id = serviceC.Insert(item);
                    }
                    i++;
                    OrderService serviceO = new OrderService();
                    int ancestor_ids5 = taxa.results[0].ancestor_ids[i];
                    RetourTaxaApi retourTaxaApiO = InaturalistApiTaxo.GetDetailAsync(ancestor_ids5).Result;
                    Order order = serviceO.GetAll().Where(king => king.Name == retourTaxaApi.results[0].name).FirstOrDefault();
                    bool dejaDansDb5 = order != null;
                    if (!dejaDansDb5)
                    {

                        order = new Order
                        {
                            ClassId = item.Id,
                            Name = retourTaxaApi.results[0].preferred_common_name,
                            LatinName = retourTaxaApi.results[0].name
                        };
                        order.Id = serviceO.Insert(order);

                    }
                    i++;
                    FamilyService serviceF = new FamilyService();
                    int ancestor_ids6 = taxa.results[0].ancestor_ids[i];
                    RetourTaxaApi retourTaxaApiF = InaturalistApiTaxo.GetDetailAsync(ancestor_ids6).Result;
                    Family family = serviceF.GetAll().Where(king => king.Name == retourTaxaApi.results[0].name).FirstOrDefault();
                    bool dejaDansDb6 = branch != null;
                    if (!dejaDansDb6)
                    {

                        family = new Family
                        {
                            OrderId = order.Id,
                            Name = retourTaxaApi.results[0].preferred_common_name,
                            LatinName = retourTaxaApi.results[0].name
                        };
                        family.Id = serviceF.Insert(family);
                    }
                    i++;
                    GenderService serviceG = new GenderService();
                    int ancestor_ids7 = taxa.results[0].ancestor_ids[i];
                    RetourTaxaApi retourTaxaApiG = InaturalistApiTaxo.GetDetailAsync(ancestor_ids7).Result;
                    Gender gender = serviceG.GetAll().Where(king => king.Name == retourTaxaApi.results[0].name).FirstOrDefault();
                    bool dejaDansDb7 = branch != null;
                    if (!dejaDansDb7)
                    {

                        gender = new Gender
                        {
                            FamilyId = family.Id,
                            Name = retourTaxaApi.results[0].preferred_common_name,
                            LatinName = retourTaxaApi.results[0].name
                        };
                        gender.Id = serviceG.Insert(gender);
                    }

                    //insérer la specie

                    SpeciesService serviceSpecie = new SpeciesService();
                    WebClient client = new WebClient();

                    byte[] pic = client.DownloadData(taxa.results[0].default_photo.medium_url);
                    Species species = new Species
                    {
                        Name = taxa.results[0].preferred_common_name,
                        LatinName = taxa.results[0].name,
                        StatusId = 1,
                        Description = taxa.results[0].wikipedia_summary,
                        GenderId = gender.Id,
                        //TODO ajouter champ photo
                        //Picture = pic
                    };
                    species.Id = serviceSpecie.Insert(species);

                }

            }


            Console.ReadKey();
        }
    }
}

