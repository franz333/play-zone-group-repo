﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_Connection
{
	public class Parameter
	{
		public object Value { get; set; }

		public DbType? Type { get; set; }
	}
}
