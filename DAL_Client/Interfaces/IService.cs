﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Client.Interfaces
{
	public interface IService<TEntity>
	{
		IEnumerable<TEntity> GetAll();

		TEntity Get(int id);

		int Insert(TEntity entity);

		bool Update(TEntity entity);

		bool Delete(int id);
	}
}
