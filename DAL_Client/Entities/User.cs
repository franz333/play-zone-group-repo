﻿using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Client.Entities
{
	public class User
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

        // Changement de type car hashage du pwd et méthode de hashage dans le set
        private byte[] _pwd;

        public byte[] Pwd
        {
            get { return _pwd; }
            set { _pwd = value; }
        }

        public string Login { get; set; }

		public bool Active { get; set; }

		public string RoleType { get; set; }

		public int RoleId { get; set; }
		
		private Role _role;

		public Role Role
		{
			get 
			{
				if (_role == null)
				{
					RoleService service = new RoleService();
					_role = service.Get(RoleId);
				}
				return _role; 
			}
		}

		// UserRole
		//private Role _role;

		//public Role Role
		//{
		//	get 
		//	{
		//		RoleService service = new RoleService();
		//		_role = service.Get(RoleId);
		//		return _role; 
		//	}
		//	set { _role = value; }
		//}

	}
}
