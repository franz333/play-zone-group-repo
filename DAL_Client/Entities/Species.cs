﻿using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Client.Entities
{
    public class Species : Kingdom
    {
		public string Description { get; set; }

		public int PeriodBegin { get; set; }

		public int PeriodEnd { get; set; }

		public int StatusId { get; set; }

		public int PublisherId { get; set; }

		public int GenderId { get; set; }
    }
}
