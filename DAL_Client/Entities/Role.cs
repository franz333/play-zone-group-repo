﻿using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL_Client.Entities
{
	public class Role
	{
		public int Id { get; set; }

		public string Type { get; set; }
	}
}
