﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Client.Entities
{
	public class Observation : Kingdom
	{
        public DateTime DateOfPost { get; set; }

        public DateTime DateOfObs { get; set; }

        public string Content { get; set; }

        public double? Longitude { get; set; }

        public double? Latitude { get; set; }

        public int SpeciesId { get; set; }

        public string SpeciesName { get; set; }

        public int StatusId { get; set; }

        public string StatusType { get; set; }

        public int ValidatorId { get; set; }

        public string ValidatorName { get; set; }

        public string ValidatorLastName { get; set; }

        public string RoleType { get; set; }

        public int PublisherId { get; set; }

        public string PublisherName { get; set; }

        public string PublisherLastName { get; set; }

        public int ObserverId { get; set; }

        public int ImgId { get; set; }

        public byte[] Img { get; set; }
    }
}
