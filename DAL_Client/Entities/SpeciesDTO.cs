﻿using DAL_Client.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Client.Entities
{
	public class SpeciesDTO : Kingdom
	{

		// Species Details
		// ----------------

		public string Species 
		{
			get 
			{
				if (Name != null)
				{
					return LatinName + " (" + Name + ")"; 
				}
				return LatinName;
			}
		}

		public string Description { get; set; }

		public int PeriodBegin { get; set; }

		public int PeriodEnd { get; set; }

		public string Period
		{
			get
			{
				return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(PeriodBegin) + " > " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(PeriodEnd);
			}
		}

		public int PublisherId { get; set; }

		public string PublisherName { get; set; }

		public string PublisherLastName { get; set; }

		private string _publisher;

		public string Publisher
		{
			get
			{
				return PublisherName + " " + PublisherLastName;
			}
			set { _publisher = value; }
		}

		public int StatusId { get; set; }

		public string StatusType { get; set; }

		
		// Taxonomy Section
		// ----------------

		public int KingdomId { get; set; }

		public string Kingdom
		{
			get
			{
				if (KingdomName != null)
				{
					return KingdomLatinName + " (" + KingdomName + ")";
				}
				return KingdomLatinName;
			}
		}

		public string KingdomLatinName { get; set; }

		public string KingdomName { get; set; }

		public int BranchId { get; set; }

		public string Branch
		{
			get
			{
				if (BranchName != null)
				{
					return BranchLatinName + " (" + BranchName + ")";
				}
				return BranchLatinName;
			}
		}

		public string BranchLatinName { get; set; }

		public string BranchName { get; set; }

		public int SubBranchId { get; set; }

		public string SubBranch
		{
			get
			{
				if (SubBranchName != null)
				{
					return SubBranchLatinName + " (" + SubBranchName + ")";
				}
				return SubBranchLatinName;
			}
		}

		public string SubBranchLatinName { get; set; }

		public string SubBranchName { get; set; }

		public int ClassId { get; set; }

		public string Class
		{
			get
			{
				if (ClassName != null)
				{
					return ClassLatinName + " (" + ClassName + ")";
				}
				return ClassLatinName;
			}
		}

		public string ClassLatinName { get; set; }

		public string ClassName { get; set; }

		public int OrderId { get; set; }

		public string Order
		{
			get
			{
				if (OrderName != null)
				{
					return OrderLatinName + " (" + OrderName + ")";
				}
				return OrderLatinName;
			}
		}

		public string OrderLatinName { get; set; }

		public string OrderName { get; set; }

		public int FamilyId { get; set; }

		public string Family
		{
			get
			{
				if (FamilyName != null)
				{
					return FamilyLatinName + " (" + FamilyName + ")";
				}
				return FamilyLatinName;
			}
		}

		public string FamilyLatinName { get; set; }

		public string FamilyName { get; set; }

		public int GenderId { get; set; }

		public string Gender
		{
			get
			{
				if (GenderName != null)
				{
					return GenderLatinName + " (" + GenderName + ")";
				}
				return GenderLatinName;
			}
		}


		public string GenderLatinName { get; set; }

		public string GenderName { get; set; }

		// Images Section

		private IEnumerable<Img> _imgs;

		public IEnumerable<Img> Imgs
		{
			get
			{
				if (_imgs == null)
				{
					ImgService service = new ImgService();
					_imgs = service.GetAllImgBySpeciesId(Id);
				}
				return _imgs;
			}
		}
	}
}
