﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Client.Entities
{
    public class TaxonomyBase
    {
        [Key]
        public int Id { get; set; }
        public string LatinName { get; set; }
        public string Name { get; set; }
    }
}
