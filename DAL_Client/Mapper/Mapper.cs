﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CE = DAL_Client.Entities;
using GE = DAL_Global.Entities;

namespace DAL_Client.Mapper
{
	internal static class Mapper
	{
		public static CE.User ToClient(this GE.User user)
		{
			return (user == null) ? null : new CE.User()
			{
				Active = user.Active,
				Email = user.Email,
				Id = user.Id,
				LastName = user.LastName,
				Login = user.Login,
				Name = user.Name,
				Pwd = user.Pwd,
				RoleId = user.RoleId,
				RoleType = user.RoleType,
			};
		}

		internal static GE.User ToGlobal(this CE.User user)
		{
			return (user == null) ? null : new GE.User()
			{
				Active = user.Active,
				Email = user.Email,
				Id = user.Id,
				LastName = user.LastName,
				Login = user.Login,
				Name = user.Name,
				Pwd = user.Pwd,
				RoleId = user.RoleId,
				RoleType = user.RoleType,
			};
		}

		public static CE.Role ToClient(this GE.Role role)
		{
			return (role == null) ? null : new CE.Role()
			{
				Id = role.Id,
				Type = role.Type,
			};  
		}

		public static CE.SpeciesDTO ToClientDTO(this GE.SpeciesDTO species)
		{
            return (species == null) ? null : new CE.SpeciesDTO()
            {
                Id = species.Id,
                LatinName = species.LatinName,
				Name = species.Name,
				Description = species.Description,
				PeriodBegin = species.PeriodBegin,
				PeriodEnd = species.PeriodEnd,
				StatusId = species.StatusId,
				StatusType = species.StatusType,
				PublisherId = species.PublisherId,
				PublisherName = species.PublisherName,
				PublisherLastName = species.PublisherLastName,
				GenderId = species.GenderId,
				GenderLatinName = species.GenderLatinName,
				GenderName = species.GenderName,
				OrderId = species.OrderId,
				OrderLatinName = species.OrderLatinName,
				OrderName = species.OrderName,
				FamilyId = species.FamilyId,
				FamilyLatinName = species.FamilyLatinName,
				FamilyName = species.FamilyName,
				ClassId = species.ClassId,
				ClassLatinName = species.ClassLatinName,
				ClassName = species.ClassName,
				SubBranchId = species.SubBranchId,
				SubBranchLatinName = species.SubBranchLatinName,
				SubBranchName = species.SubBranchName,
				BranchId = species.BranchId,
				BranchLatinName = species.BranchLatinName,
				BranchName = species.BranchName,
				KingdomId = species.KingdomId,
				KingdomLatinName = species.KingdomLatinName,
				KingdomName = species.KingdomName,
			};
		}

		public static CE.Species ToClient(this GE.Species species)
		{
			return (species == null) ? null : new CE.Species()
			{
				Description = species.Description,
				GenderId = species.GenderId,
				LatinName = species.LatinName,
				Name = species.Name,
				PeriodBegin = species.PeriodBegin,
				PeriodEnd = species.PeriodEnd,
				Id = species.Id,
				PublisherId = species.PublisherId,
				StatusId = species.StatusId,
			};
		}

        public static GE.Species ToGlobal(this CE.Species species)
		{
			return (species == null) ? null : new GE.Species()
			{
				Description = species.Description,
				GenderId = species.GenderId,
				LatinName = species.LatinName,
				Name = species.Name,
				PeriodBegin = species.PeriodBegin,
				PeriodEnd = species.PeriodEnd,
				Id = species.Id,
				PublisherId = species.PublisherId,
				StatusId = species.StatusId,
			};
		}

		public static CE.Kingdom ToClient(this GE.Kingdom kingdom)
		{
			return (kingdom == null) ? null : new CE.Kingdom()
			{
				Id = kingdom.Id,
				LatinName = kingdom.LatinName,
				Name = kingdom.Name,
			};
		}

		public static GE.Kingdom ToGlobal(this CE.Kingdom kingdom)
		{
			return (kingdom == null) ? null : new GE.Kingdom()
			{
				Id = kingdom.Id,
				LatinName = kingdom.LatinName,
				Name = kingdom.Name,
			};
		}

		public static CE.Branch ToClient(this GE.Branch branch) 
		{
			return (branch == null) ? null : new CE.Branch()
			{
				Id = branch.Id,
				KingdomId = branch.KingdomId,
				LatinName = branch.LatinName,
				Name = branch.Name,
			};
		}

		public static GE.Branch ToGlobal(this CE.Branch branch)
		{
			return (branch == null) ? null : new GE.Branch()
			{
				Id = branch.Id,
				KingdomId = branch.KingdomId,
				LatinName = branch.LatinName,
				Name = branch.Name,
			};
		}

		public static GE.SubBranch ToGlobal(this CE.SubBranch subBranch)
		{
			return (subBranch == null) ? null : new GE.SubBranch()
			{
				Id = subBranch.Id,
				LatinName = subBranch.LatinName,
				Name = subBranch.Name,
				BranchId = subBranch.BranchId,
			};
		}

		public static CE.SubBranch ToClient(this GE.SubBranch subBranch)
		{ 
			return (subBranch == null) ? null : new CE.SubBranch()
			{
				Id = subBranch.Id,
				LatinName = subBranch.LatinName,
				Name = subBranch.Name,
				BranchId = subBranch.BranchId,
			};
		}

		public static CE.Class ToClient(this GE.Class spClass)
		{
			return (spClass == null) ? null : new CE.Class()
			{
				LatinName = spClass.LatinName,
				Name = spClass.Name,
				BranchId = spClass.BranchId,
				SubBranchId = spClass.SubBranchId,
				Id = spClass.Id
			};  
		}

		public static GE.Class ToGlobal(this CE.Class spClass)
		{
			return (spClass == null) ? null : new GE.Class()
			{
				LatinName = spClass.LatinName,
				Name = spClass.Name,
				BranchId = spClass.BranchId,
				SubBranchId = spClass.SubBranchId,
				Id = spClass.Id
			};  
		}

		public static CE.Order ToClient(this GE.Order order)
		{
			return (order == null) ? null : new CE.Order()
			{
				LatinName = order.LatinName,
				Name = order.Name,
				Id = order.Id,
				ClassId = order.ClassId,
			}; 
		}

		public static GE.Order ToGlobal(this CE.Order order)
		{
			return (order == null) ? null : new GE.Order()
			{
				LatinName = order.LatinName,
				Name = order.Name,
				Id = order.Id,
				ClassId = order.ClassId,
			};
		}

		public static CE.Family ToClient(this GE.Family family)
		{
			return (family == null) ? null : new CE.Family()
			{
				Id = family.Id,
				LatinName = family.LatinName,
				Name = family.Name,
				OrderId = family.OrderId,
			};
		}

		public static GE.Family ToGlobal(this CE.Family family)
		{
			return (family == null) ? null : new GE.Family()
			{
				Id = family.Id,
				LatinName = family.LatinName,
				Name = family.Name,
				OrderId = family.OrderId,
			};
		}

		public static CE.Gender ToClient(this GE.Gender gender)
		{
			return (gender == null) ? null : new CE.Gender()
			{
				Id = gender.Id,
				LatinName = gender.LatinName,
				Name = gender.Name,
				FamilyId = gender.FamilyId,
			};
		}

		public static GE.Gender ToGlobal(this CE.Gender gender)
		{
			return (gender == null) ? null : new GE.Gender()
			{
				Id = gender.Id,
				LatinName = gender.LatinName,
				Name = gender.Name,
				FamilyId = gender.FamilyId,
			};
		}

		public static CE.Img ToClient(this GE.Img img)
		{
            return (img == null) ? null : new CE.Img()
            {
                Id = img.Id,
                Date = img.Date,
                Data = img.Data,
                Origin = img.Origin,
                CopyRight = img.CopyRight,
                ObservationId = img.ObservationId,
                SpeciesId = img.SpeciesId,
                Format = img.Format,
			};
		}

		public static GE.Img ToGlobal(this CE.Img img)
		{
			return (img == null) ? null : new GE.Img()
			{
				Id = img.Id,
				Date = img.Date,
				Data = img.Data,
				Origin = img.Origin,
				CopyRight = img.CopyRight,
				ObservationId = img.ObservationId,
				SpeciesId = img.SpeciesId,
                Format = img.Format,
			};
		}

		public static CE.Status ToClient(this GE.Status status)
		{
			return (status == null) ? null : new CE.Status()
			{
				Id = status.Id,
				Type = status.Type,
			};
		}

		public static GE.Status ToGlobal(this CE.Status status)
		{
			return (status == null) ? null : new GE.Status()
			{
				Id = status.Id,
				Type = status.Type,
			};
		}

        public static CE.Observation ToClient(this GE.Observation observation)
        {
            return (observation == null) ? null : new CE.Observation()
            {
                Id = observation.Id,
                DateOfObs = observation.DateOfObs,
                DateOfPost = observation.DateOfPost,
                Content = observation.Content,
                Latitude = observation.Latitude,
                Longitude = observation.Longitude,
                SpeciesName = observation.SpeciesName,
                SpeciesId = observation.SpeciesId,
                StatusId = observation.StatusId,
                ValidatorId = observation.ValidatorId,
                PublisherId = observation.PublisherId,
                ObserverId = observation.ObserverId,
                Img = observation.Img,
                ImgId = observation.ImgId,
                ValidatorLastName = observation.ValidatorLastName,
                ValidatorName = observation.ValidatorName,
                PublisherLastName = observation.PublisherLastName,
                PublisherName = observation.PublisherName,
                StatusType = observation.StatusType,
                RoleType = observation.RoleType,                
            };
        }

        public static GE.Observation ToGlobal(this CE.Observation observation)
        {
            return (observation == null) ? null : new GE.Observation()
            {
                DateOfObs = observation.DateOfObs,
                DateOfPost = observation.DateOfPost,
                Content = observation.Content,
                Longitude = observation.Longitude,
                Latitude = observation.Latitude,
                SpeciesName = observation.SpeciesName,
                SpeciesId = observation.SpeciesId,
                StatusId = observation.StatusId,
                ValidatorId = observation.ValidatorId,
                PublisherId = observation.PublisherId,
                ObserverId = observation.ObserverId,
            };
        }

        //#region API MAPPER
        //public static CE.SpeciesAPI ToClientAPI(this GE.Species species)
        //{
        //    return (species == null) ? null : new CE.SpeciesAPI()
        //    {
        //        Id = species.Id,
        //        LatinName = species.LatinName,
        //        Name = species.Name,
        //        Description = species.Description,
        //        PeriodBegin = species.PeriodBegin,
        //        PeriodEnd = species.PeriodEnd,
        //        GenderId = species.GenderId,
        //        GenderLatinName = species.GenderLatinName,
        //        GenderName = species.GenderName,
        //        OrderId = species.OrderId,
        //        OrderLatinName = species.OrderLatinName,
        //        OrderName = species.OrderName,
        //        FamilyId = species.FamilyId,
        //        FamilyLatinName = species.FamilyLatinName,
        //        FamilyName = species.FamilyName,
        //        ClassId = species.ClassId,
        //        ClassLatinName = species.ClassLatinName,
        //        ClassName = species.ClassName,
        //        SubBranchId = species.SubBranchId,
        //        SubBranchLatinName = species.SubBranchLatinName,
        //        SubBranchName = species.SubBranchName,
        //        BranchId = species.BranchId,
        //        BranchLatinName = species.BranchLatinName,
        //        BranchName = species.BranchName,
        //        KingdomId = species.KingdomId,
        //        KingdomLatinName = species.KingdomLatinName,
        //        KingdomName = species.KingdomName,
        //    };
        //}
        //#endregion


    }
}
