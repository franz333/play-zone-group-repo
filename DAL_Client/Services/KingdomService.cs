﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class KingdomService : IService<Kingdom>
	{
		private GS.KingdomService repositotyGlobal;

		public KingdomService()
		{
			repositotyGlobal = new GS.KingdomService();
		}
		
		public bool Delete(int id)
		{
            return repositotyGlobal.Delete(id);
		}

		public Kingdom Get(int id)
		{
			return repositotyGlobal.Get(id).ToClient();
		}

		public IEnumerable<Kingdom> GetAll()
		{
			return repositotyGlobal.GetAll().Select(k => k.ToClient());
		}

		public int Insert(Kingdom entity)
		{
			return repositotyGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Kingdom entity)
		{
            return repositotyGlobal.Update(entity.ToGlobal());
		}
	}
}
