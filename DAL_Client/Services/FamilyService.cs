﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class FamilyService : IService<Family>
	{
		private GS.FamilyService repositoryGlobal;

		public FamilyService()
		{
			repositoryGlobal = new GS.FamilyService();
		}

		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public Family Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Family> GetAll()
		{
			return repositoryGlobal.GetAll().Select(f => f.ToClient());
		}

		public IEnumerable<Family> GetAllFamilyByOrderId(int orderId)
		{
			return repositoryGlobal.GetAllFamilyByOrderId(orderId).Select(f => f.ToClient());
		}

		public int Insert(Family entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Family entity)
		{
            return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
