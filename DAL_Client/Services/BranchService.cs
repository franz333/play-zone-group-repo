﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class BranchService : IService<Branch>
	{
		private GS.BranchService repositoryGlobal;

		public BranchService()
		{
			repositoryGlobal = new GS.BranchService();
		}
  
		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public Branch Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Branch> GetAll()
		{
			return repositoryGlobal.GetAll().Select(b => b.ToClient());
		}

		public IEnumerable<Branch> GetAllBranchByKingdomId(int KingdomId)
		{
			return repositoryGlobal.GetAllBranchByKingdomId(KingdomId).Select(b => b.ToClient());
		}

		public int Insert(Branch entity)
		{
            return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Branch entity)
		{
            return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
