﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class ClassService : IService<Class>
	{
		private GS.ClassService repositoryGlobal;

		public ClassService()
		{
			repositoryGlobal = new GS.ClassService();
		}
  
		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public Class Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Class> GetAll()
		{
			return repositoryGlobal.GetAll().Select(c => c.ToClient());
		}

		public IEnumerable<Class> GetAllClassByBranchId(int branchId)
		{
			return repositoryGlobal.GetAllClassByBranchId(branchId).Select(c => c.ToClient());		
		}

		public IEnumerable<Class> GetAllClassBySubBranchId(int subBranchId)
		{
			return repositoryGlobal.GetAllClassBySubBranchId(subBranchId).Select(c => c.ToClient());
		}

		public int Insert(Class entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Class entity)
		{
            return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
