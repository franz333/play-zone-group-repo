﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class StatusService : IService<Status>
	{
		private GS.StatusService repositoryGlobal;

		public StatusService()
		{
			repositoryGlobal = new GS.StatusService();
		}

        // Géré en SQL si besoin, mais cela ne devrait pas être nécessaire (status fixés au départ)
        public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public Status Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Status> GetAll()
		{
			return repositoryGlobal.GetAll().Select(s => s.ToClient());
		}

        // Géré en SQL si besoin, mais cela ne devrait pas être nécessaire (status fixés au départ)
        public int Insert(Status entity)
		{
			throw new NotImplementedException();
		}

        // Géré en SQL si besoin, mais cela ne devrait pas être nécessaire (status fixés au départ)
        public bool Update(Status entity)
		{
			throw new NotImplementedException();
		}
	}
}
