﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using DAL_Client.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GS = DAL_Global.Services;

namespace DAL_Client.Services
{
	public class RoleService : IService<Role>
	{
		private GS.RoleService repositoryGlobal;

		public RoleService()
		{
			 repositoryGlobal = new GS.RoleService();
		}

		// Géré en SQL si besoin, mais cela ne devrait pas être nécessaire (roles fixés au départ)
		public bool Delete(int id)
		{
			throw new NotImplementedException();
		}

		public Role Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Role> GetAll()
		{
			return repositoryGlobal.GetAll().Select(r => r.ToClient());
		}

		// Géré en SQL à la création de la DB (roles fixés au départ)
		public int Insert(Role entity)
		{
			throw new NotImplementedException();
		}

		// Géré en SQL si besoin, mais cela ne devrait pas être nécessaire (roles fixés au départ)
		public bool Update(Role entity)
		{
			throw new NotImplementedException();
		}
	}
}
