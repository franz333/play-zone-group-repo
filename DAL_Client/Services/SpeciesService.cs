﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using DAL_Client.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GS = DAL_Global.Services;

namespace DAL_Client.Services
{
    public class SpeciesService : IService<Species>
	{
		private GS.SpeciesService repositoryGlobal;

		public SpeciesService()
		{
			repositoryGlobal = new GS.SpeciesService();
		}

		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public Species Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public SpeciesDTO GetDTO(int key)
		{
			return repositoryGlobal.GetDTO(key).ToClientDTO();
		}

		public IEnumerable<Species> GetAll()
		{
			IEnumerable<Species> species = repositoryGlobal.GetAll().Select(s => s.ToClient());
			return species;
		}

		public IEnumerable<SpeciesDTO> GetAllDTO()
		{
			return repositoryGlobal.GetAllDTO().Select(spDTO => spDTO.ToClientDTO());
		}

		public IEnumerable<SpeciesDTO> GetAllByStatusId(int statusId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllByStatusId(statusId).Select(s => s.ToClientDTO());
            return species;
        }

        public IEnumerable<SpeciesDTO> GetAllByPublisherId(int publisherId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllByPublisherId(publisherId).Select(s => s.ToClientDTO());
            return species;
        }

        public IEnumerable<SpeciesDTO> GetAllSpeciesByPeriod(int periodBegin, int periodEnd)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesByPeriod(periodBegin, periodEnd).Select(s => s.ToClientDTO());
            return species;
        }

        public IEnumerable<SpeciesDTO> GetAllSpeciesByKingdomId(int kingdomId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesByKingdomId(kingdomId).Select(s => s.ToClientDTO());
            return species;
        }

        public IEnumerable<SpeciesDTO> GetAllSpeciesByBranchId(int branchId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesByBranchId(branchId).Select(s => s.ToClientDTO());
            return species;
        }

        public IEnumerable<SpeciesDTO> GetAllSpeciesBySubBranchId(int subBranchId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesBySubBranchId(subBranchId).Select(s => s.ToClientDTO());
            return species;
        }        

        public IEnumerable<SpeciesDTO> GetAllSpeciesByClassId(int classId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesByClassId(classId).Select(s => s.ToClientDTO());
            return species;
        }

        public IEnumerable<SpeciesDTO> GetAllSpeciesByOrderId(int orderId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesByOrderId(orderId).Select(s => s.ToClientDTO());
            return species;
        }
        public IEnumerable<SpeciesDTO> GetAllSpeciesByFamilyId(int familyId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesByFamilyId(familyId).Select(s => s.ToClientDTO());
            return species;
        }

        public IEnumerable<SpeciesDTO> GetAllSpeciesByGenderId(int genderId)
        {
            IEnumerable<SpeciesDTO> species = repositoryGlobal.GetAllSpeciesByGenderId(genderId).Select(s => s.ToClientDTO());
            return species;
        }

        public int Insert(Species entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Species entity)
		{
			return repositoryGlobal.Update(entity.ToGlobal());
		}
    }
}
