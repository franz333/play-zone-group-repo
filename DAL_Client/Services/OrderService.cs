﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class OrderService : IService<Order>
	{
		private GS.OrderService repositoryGlobal;

		public OrderService()
		{
			repositoryGlobal = new GS.OrderService();
		}
  
		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public Order Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Order> GetAll()
		{
			return repositoryGlobal.GetAll().Select(o => o.ToClient());
		}

		public IEnumerable<Order> GetAllOrderByClassId(int classId)
		{
			return repositoryGlobal.GetAllOrderByClassId(classId).Select(o => o.ToClient());
		}

		public int Insert(Order entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Order entity)
		{
            return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
