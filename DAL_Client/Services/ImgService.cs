﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class ImgService : IService<Img>
	{
		private GS.ImgService repositoryGlobal;

		public ImgService()
		{
			repositoryGlobal = new GS.ImgService();
		}

		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public Img Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public Img GetImgByObservationId(int observationId)
		{
			return repositoryGlobal.GetImgByObservationId(observationId).ToClient();
		}

		public IEnumerable<Img> GetAll()
		{
			return repositoryGlobal.GetAll().Select(i => i.ToClient());
		}

		public IEnumerable<Img> GetAllImgBySpeciesId(int speciesId)
		{
			return repositoryGlobal.GetAllImgBySpeciesId(speciesId).Select(i => i.ToClient());
		}

		public int Insert(Img entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Img entity)
		{
            return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
