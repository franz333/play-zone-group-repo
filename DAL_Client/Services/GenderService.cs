﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class GenderService : IService<Gender>
	{
		private GS.GenderService repositoryGlobal;

		public GenderService()
		{
			repositoryGlobal = new GS.GenderService();
		}

		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public Gender Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Gender> GetAll()
		{
			return repositoryGlobal.GetAll().Select(g => g.ToClient());
		}

		public IEnumerable<Gender> GetAllGenderByFamilyId(int familyId)
		{
			return repositoryGlobal.GetAllGenderByFamilyId(familyId).Select(f => f.ToClient());
		}

		public int Insert(Gender entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Gender entity)
		{
            return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
