﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using DAL_Client.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GS = DAL_Global.Services;

namespace DAL_Client.Services
{
	public class UserService : IService<User>
	{
		private GS.UserService repositoryGlobal;

		public UserService()
		{
			repositoryGlobal = new GS.UserService();
		}

		public bool Delete(int id)
		{
			return repositoryGlobal.Delete(id);
		}

		public IEnumerable<User> GetAll()
		{
			IEnumerable<User> users = repositoryGlobal.GetAll().Select(u => u.ToClient());
			return users;
		}

		public User Get(int id)
		{
			User user = repositoryGlobal.Get(id).ToClient();
			return user;
		}

		public User Login(User user)
		{
			User userLogged = new User();
			userLogged = repositoryGlobal.Login(user.ToGlobal()).ToClient();

			if (userLogged != null)
			{				
				return userLogged;
			}
			else
			{
				// TODO : comment gérer ce type d'erreur?
				return null;
			}
		}

		public int Insert(User entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(User entity)
		{
			return repositoryGlobal.Update(entity.ToGlobal());
		}

		public bool Deactivate(int id)
		{
			return repositoryGlobal.Deactivate(id);
		}

        public bool Activate(int id)
        {
            return repositoryGlobal.Activate(id);
        }
	}
}
