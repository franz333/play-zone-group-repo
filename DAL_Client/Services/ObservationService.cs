﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using GS = DAL_Global.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class ObservationService : IService<Observation>
	{
		private GS.ObservationService repositoryGlobal;

		public ObservationService()
		{
			repositoryGlobal = new GS.ObservationService();
		}

		public bool Delete(int id)
		{
			return repositoryGlobal.Delete(id);
		}

		public Observation Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<Observation> GetAll()
		{
			return repositoryGlobal.GetAll().Select(o => o.ToClient());
		}

        public IEnumerable<Observation> GetAllByObserverId(int observerId)
        {
            return repositoryGlobal.GetAllByObserverId(observerId).Select(o => o.ToClient());
        }

        public IEnumerable<Observation> GetAllByValidatorId(int validatorId)
        {
            return repositoryGlobal.GetAllByValidatorId(validatorId).Select(o => o.ToClient());
        }

        public IEnumerable<Observation> GetAllByPublisherId(int publisherId)
        {
            return repositoryGlobal.GetAllByPublisherId(publisherId).Select(o => o.ToClient());
        }

        public int Insert(Observation entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal());
		}

		public bool Update(Observation entity)
		{
			return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
