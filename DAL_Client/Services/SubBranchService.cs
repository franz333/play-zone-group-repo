﻿using DAL_Client.Entities;
using DAL_Client.Interfaces;
using System;
using System.Collections.Generic;
using GS = DAL_Global.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Client.Mapper;

namespace DAL_Client.Services
{
	public class SubBranchService : IService<SubBranch>
	{
		private GS.SubBranchService repositoryGlobal;

		public SubBranchService()
		{
			repositoryGlobal = new GS.SubBranchService();
		}

		public bool Delete(int id)
		{
            return repositoryGlobal.Delete(id);
		}

		public SubBranch Get(int id)
		{
			return repositoryGlobal.Get(id).ToClient();
		}

		public IEnumerable<SubBranch> GetAll()
		{
			return repositoryGlobal.GetAll().Select(sb => sb.ToClient());
		}

		public IEnumerable<SubBranch> GetAllSubBranchByBranchId(int branchId)
		{
			return repositoryGlobal.GetAllSubBranchByBranchId(branchId).Select(sb => sb.ToClient());
		}

		public int Insert(SubBranch entity)
		{
			return repositoryGlobal.Insert(entity.ToGlobal()); 
		}

		public bool Update(SubBranch entity)
		{
            return repositoryGlobal.Update(entity.ToGlobal());
		}
	}
}
